<?php
/**
 * @file
 *   The hook callbacks that this package uses.
 */


/**
 * Return the formats this module can process.
 *
 * The list of format info is used to present the Admin UI, and defined the
 * processes behind reading the named format.
 */
function HOOK_field_schema_importer_formats() {
  $formats = array(
    'php' => array(

      // ID should be the same as the key.
      'id' => 'php',

      // Name is shown in the UI form and some logs.
      'name' => 'Direct PHP export dump',

      // Description may be displayed in the UI form.
      // It would help if it links to both a formal specification,
      // and some examples.
      'description' => t("PHP configuration array, as exported by bundle_copy.module."),

      // Name this module, used mostly for debug messages.
      'module' => 'field_schema_importer',

      // The parser_callback function is always expected to take a source
      // string, and return an ARRAY of content types.
      // As different source strings may provide more than one 'content type'
      // always return an array, even if it's just one.
      // @see hook_field_schema_importer_parser_callback()
      'parser_callback' => 'field_schema_importer_parse_php',

      // @see hook_field_schema_importer_processor_callback()
      'processor_callback' => 'field_schema_importer_create_bundles',
    ),
  );
  return $formats;
}
