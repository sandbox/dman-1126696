<?php
/**
 * Code stolen from bundle_copy.module.
 *
 * Helper functions to programatically save a content type definition
 * from a PHP config array.
 *
 * This is because the structure of bundle_copy.module could not be
 * easily adapted to being called as just a CRUD tool, and therefore wasn't
 * useful enough on its own as becoming a dependency, BUT had all of the code
 * I wanted to use.
 *
 * So here are some of the functions - still in the bundle_copy namespace
 * I pirated. This stub should only be included when bundle_copy.module
 * is NOT available or it will conflict.
 *
 **/



/**
 * Api function to get the bundle copy info.
 *
 * Stolen from bundle_info and compressed.
 * TODO revisit.
 */
function bundle_copy_get_info() {
  static $info = FALSE;

  if ($info) {
    return $info;
  }

  $info['node'] = array(
    'bundle_export_callback' => 'node_type_get_type',
    'bundle_save_callback' => 'node_type_save',
    'export_menu' => array(
      'path' => 'admin/structure/types/export',
      'access arguments' => 'administer content types',
    ),
    'import_menu' => array(
      'path' => 'admin/structure/types/import',
      'access arguments' => 'administer content types',
    ),
  );

  $info['user'] = array(
    'bundle_export_callback' => '_bc_bundle_export_ignore',
    'bundle_save_callback' => '_bc_bundle_save_ignore',
    'export_menu' => array(
      'path' => 'admin/config/people/accounts/export',
      'access arguments' => 'administer users',
    ),
    'import_menu' => array(
      'path' => 'admin/config/people/accounts/import',
      'access arguments' => 'administer users',
    ),
  );

  if (module_exists('taxonomy')) {
    $info['taxonomy_term'] = array(
      'bundle_export_callback' => '_bc_copy_taxonomy_load',
      'bundle_save_callback' => '_bc_copy_taxonomy_save',
      'export_menu' => array(
        'path' => 'admin/structure/taxonomy/export',
        'access arguments' => 'administer taxonomy',
      ),
      'import_menu' => array(
        'path' => 'admin/structure/taxonomy/import',
        'access arguments' => 'administer taxonomy',
      ),
    );
  }

  return $info;
}

/**
 * Code below here is VERBATIM from bundle_copy.module, and wrapped clumsily by
 * field_schema_importer_create_bundle()
 */

/**
 * Submit callback: import data.
 */
function bundle_copy_import_submit($form, &$form_state) {

  // Evaluate data.
  eval($form_state['values']['macro']);

  if (isset($data) && is_array($data)) {

    $modules = module_list();
    $bc_info = bundle_copy_get_info();
    $strings = array();

    // Create bundles.
    foreach ($data['bundles'] as $key => $bundle) {
      $entity_type = '';
      if (is_object($bundle)) {
        $entity_type = $bundle->bc_entity_type;
      }
      elseif (is_array($bundle)) {
        $entity_type = $bundle['bc_entity_type'];
      }
      if (!empty($entity_type)) {
        $existing_bundles = _bundle_copy_bundle_info($entity_type);
        $bundle_save_callback = $bc_info[$entity_type]['bundle_save_callback'];
        if (! function_exists($bundle_save_callback)) {
          drupal_set_message("Problem saving bundle. No save callback known for $entity_type" . print_r($bundle, 1));
          continue;
        }
        $bundle_info = $bundle_save_callback($bundle);
        dpm($bundle);
        $strings['!bundle'] = $bundle->name;
        if (!isset($existing_bundles[$key])) {
          drupal_set_message(t('!bundle bundle has been created.', $strings));
        }
        else {
          drupal_set_message(t('!bundle bundle has been updated.', $strings));
        }
      }
    }

    // Create or update fields and their instances
    if (isset($data['fields'])) {
      foreach ($data['fields'] as $key => $field) {

        // Check if the field module exists.
        $module = $field['module'];
        if (!isset($modules[$module])) {
          drupal_set_message(t('%field_name field could not be created because the module %module is disabled or missing.', array('%field_name' => $key, '%module' => $module)), 'error');
          continue;
        }

        if (isset($data['instances'][$key])) {

          // Create or update field.
          $prior_field = field_read_field($field['field_name'], array('include_inactive' => TRUE));
          if (!$prior_field) {
            field_create_field($field);
            drupal_set_message(t('%field_name field has been created.', array('%field_name' => $key)));
          }
          else {
            $field['id'] = $prior_field['id'];
            // Changing field type is illegal
            if ($field['type'] != $prior_field['type']) {
              drupal_set_message(t('%field_name field has NOT been updated. Cannot change field type from "%prior_field_type" to "%field_type"', array('%field_name' => $key, '%field_type' => $field['type'], '%prior_field_type' => $prior_field['type'])), 'error');
            }
            else {
              // If trying to save partial field update over an existing
              // one, need the gaps filled in fully or field_update_field()
              // takes exception.
              $field = array_replace_recursive($prior_field, $field);
              field_update_field($field);
              drupal_set_message(t('%field_name field has been updated.', array('%field_name' => $key)));
            }
          }

          // Create or update field instances.
          foreach ($data['instances'][$key] as $ikey => $instance) {

            // Make sure the needed key exists.
            if (!isset($instance['field_name'])) {
              continue;
            }

            $prior_instance = field_read_instance($instance['entity_type'], $instance['field_name'], $instance['bundle']);
            if (!$prior_instance) {
              field_create_instance($instance);
              drupal_set_message(t('%field_name instance has been created for @bundle in @entity_type.', array('%field_name' => $key, '@bundle' => $instance['bundle'], '@entity_type' => $instance['entity_type'])));
            }
            else {
              $instance['id'] = $prior_instance['id'];
              $instance['field_id'] = $prior_instance['field_id'];
              field_update_instance($instance);
              drupal_set_message(t('%field_name instance has been updated for @bundle in @entity_type.', array('%field_name' => $key, '@bundle' => $instance['bundle'], '@entity_type' => $instance['entity_type'])));
            }
          }
        }
      }
    }

    // Create / update fieldgroups.
    if (isset($data['fieldgroups'])) {
      if (module_exists('field_group')) {
        ctools_include('export');
        $existing_field_groups = field_group_info_groups();
        foreach ($data['fieldgroups'] as $identifier => $fieldgroup) {
          if (isset($existing_field_groups[$fieldgroup->entity_type][$fieldgroup->bundle][$fieldgroup->mode][$fieldgroup->group_name])) {
            $existing = $existing_field_groups[$fieldgroup->entity_type][$fieldgroup->bundle][$fieldgroup->mode][$fieldgroup->group_name];
            $fieldgroup->id = $existing->id;
            if (!isset($fieldgroup->disabled)) {
              $fieldgroup->disabled = FALSE;
            }
            ctools_export_crud_save('field_group', $fieldgroup);
            ctools_export_crud_set_status('field_group', $fieldgroup, $fieldgroup->disabled);
            drupal_set_message(t('%fieldgroup fieldgroup has been updated for @bundle in @entity_type.', array('%fieldgroup' => $fieldgroup->label, '@bundle' => $fieldgroup->bundle, '@entity_type' => $fieldgroup->entity_type)));
          }
          else {
            unset($fieldgroup->id);
            unset($fieldgroup->export_type);
            if (!isset($fieldgroup->disabled)) {
              $fieldgroup->disabled = FALSE;
            }
            ctools_export_crud_save('field_group', $fieldgroup);
            $fieldgroup->export_type = 1;
            ctools_export_crud_set_status('field_group', $fieldgroup, $fieldgroup->disabled);
            drupal_set_message(t('%fieldgroup fieldgroup has been saved for @bundle in @entity_type.', array('%fieldgroup' => $fieldgroup->label, '@bundle' => $fieldgroup->bundle, '@entity_type' => $fieldgroup->entity_type)));
          }
        }
      }
      else {
        drupal_set_message(t('The fieldgroups could not be saved because the <em>Field group</em> module is disabled or missing.'), 'warning');
      }
    }

    // Clear caches.
    field_info_cache_clear();
    if (module_exists('field_group')) {
      cache_clear_all('field_groups', 'cache_field');
    }
  }
  else {
    drupal_set_message(t('The pasted text did not contain any valid export data.'), 'error');
  }
}



/**
 * Return bundles for a certain entity type.
 *
 * @param $entity_type
 *   The name of the entity type.
 * @param $table_select
 *   Whether we're returning for the table select or not.
 */
function _bundle_copy_bundle_info($entity_type, $table_select = FALSE) {
  static $bundles = array();

  if (!isset($bundles[$entity_type])) {
    $bundles[$entity_type] = array();
    $entity_info = entity_get_info($entity_type);
    $entity_bundles = $entity_info['bundles'];
    ksort($entity_bundles);
    foreach ($entity_bundles as $key => $info) {
      $label = isset($info['label']) ? $info['label'] : drupal_ucfirst(str_replace('_', ' ', $key));
      if ($table_select) {
        $bundles[$entity_type][$key]['label'] = $label;
      }
      else {
        $bundles[$entity_type][$key] = $label;
      }
    }
  }

  return $bundles[$entity_type];
}
