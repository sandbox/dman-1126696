<?php
/**
 * @file
 * Utility function to import RDF Schemas into Drupal Entities.
 *
 * Requires ARC2 RDF library.
 *
 * @author dman http://coders.co.nz/
 */


/**
 * Returns an array defining one or more bundles.
 *
 * @param string $source_string
 *   Raw source of the input. May be HTML+RDFa,
 *   also turtle, RDF/XML and anything ARC2 can autodetect.
 *
 * @return array|bool
 *   Content type definition array, compatible with entity_export.
 */
function convert_rdfs_to_content_type($source_string) {

  if (!_rdfs2cck_load_arc()) {
    return "Failed to load ARC2";
  }

  $parser = rdfs2cck_parse_rdf($source_string);

  $content_types = rdfs2cck_rdfs_to_content_type($parser);

  if (empty($content_types)) {
    drupal_set_message(t("
      Problem parsing any CCK definitions from the given CCK Import.
      No top-level content types detected.
      Possibly the source was valid XML but contained no rdfs:Class definitions.
      "
    ), 'error');
    return FALSE;
  }
  return $content_types;
}


/**
 * Originally stolen from rdfx, then simplified.
 *
 * Basically calls ARC2 parser on the given data, but works in some
 * expected namespace support also.
 *
 * This is mostly housekeeping, cleanup and error handling.
 * Otherwise, think of it as ARC2::Parser::parse()
 *
 * @param string $source_string
 *   What to parse.
 *
 * @return ARC2_RDFParser
 *   A parser object.
 */
function rdfs2cck_parse_rdf($source_string) {
  // Parser autodetects content, probably SemHTMLParser.
  $config = array('auto_extract' => 0);

  /** @var ARC2_RDFParser $parser */
  $parser = ARC2::getRDFParser($config);

  // ARC2 expects a base URL for everything it parses.
  // Unfortunately with file uploads etc, this may not exist,
  // and with our pipeline, this is not in context.
  $uri = 'http://dummy.base.uri/';

  //
  // Not sure why the $parser object doesn't remember its own namespaces.
  // Re-add them here.
  // This mainly means that we can refer to things by CURIE consistently.
  $namespaces = array();

  // Drupal MAY know a few namespaces already, ensure they are what we expect.
  if (function_exists('rdf_get_namespaces')) {
    $rev_namespaces = array_flip($namespaces);
    $our_rev_namespaces = array_flip(rdf_get_namespaces());
    $namespaces = array_flip($our_rev_namespaces + $rev_namespaces);
  }
  else {
    // You don't have the rdf module?
    // We expect at least rdfs, (in order to use rdfs:Class and rdfs:Property)
    // and it's common to use schema: and owl: also.
    $namespaces['rdfs'] = 'http://www.w3.org/2000/01/rdf-schema#';
    // $namespaces['rdfa'] = 'http://www.w3.org/ns/rdfa#';
    $namespaces['owl'] = 'http://www.w3.org/2002/07/owl#';
  }
  foreach ($parser->nsp as $ns_uri => $prefix) {
    $namespaces[$prefix] = $ns_uri;
  }
  // Chop http://some.thing/path/etc into just http://some.thing/
  // This ALSO works on myself, my own absolute siteroot.
  $base_uri = preg_replace('|^([^/]*?//[^/]*)/?.*$|', '$1/', $uri);
  // These are not required technically, but much beeter to read than ns0 etc.
  $namespaces['self'] = $base_uri;
  $namespaces['schema'] = 'http://schema.org/';
  $parser->ns = $namespaces;

  // Done messing with namespaces.

  $parser->parse($uri, $source_string);
  if ($errs = $parser->getErrors()) {
    trigger_error(print_r($errs, 1));
    return $parser;
  }
  $parser->extractRDF('rdfa microformats');

  // Um, it forgot?
  $parser->ns = $namespaces;

  return $parser;
}


/**
 * Interprets an ARC2 RDF dataset and returns entity field definitions.
 *
 * Convert a set of RDFs class, and type statements into a framework
 * entity type def object as used for cck import etc.
 *
 * Returns a '$content' object that holds both type and fields - as fields
 * definitions are not nested within the type object itself, but parallal with
 * it. This is how cck export does it.
 *
 *
 * @param ARC2_RDFParser $parser
 *   A php array containing the results of the Freebase query on types.
 *
 * @return array
 *   Structure similar to that used by bundle_copy.
 */
function rdfs2cck_rdfs_to_content_type(ARC2_RDFParser $parser) {
  if (empty($parser)) {
    drupal_set_message('Null RDF parser object, No schema to convert');
    return NULL;
  }

  // Build an array that content_copy will recognise.
  $data = array();

  $triples = $parser->getTriples();
  // dpm($triples);

  $index = ARC2::getSimpleIndex($triples, FALSE);

  // Now comes the actual work - interpreting the RDFs statements
  // into Drupal Entities.

  // A thing with an rdf:type of rdfs:Class is a candidate for us
  // to convert into a Drupal Entity or content type.
  // So start with these.

  $index_by_type = rdfs2cck_query_items_by_type($parser);
  $classes = $index_by_type['rdfs:Class'];

  foreach ($classes as $class_id => $class_def) {
    $curie = $parser->getPname($class_id);
    list($ns, $id) = $parser->splitURI($curie);
    $bundle_id = rdfs2cck_machine_name($id);

    // Use the abstraction property-name-mappings when looking for field_values.
    $label = strip_tags(rdfs2cck_fetch_property($class_def, 'rdfs:label', TRUE));
    if (empty($label)) {
      $label = $id;
    }

    // Start with a placeholder bundle definition.
    $bundle = field_schema_importer_content_type_defaults($label, $bundle_id);
    $bundle_id = $bundle['type'];
    $entity_type = 'node';

    // Start adding properties.
    $comment = rdfs2cck_fetch_property($class_def, 'rdfs:comment', TRUE);
    $bundle['description'] = !empty($comment) ? $comment : '';

    // Bundle_copy needs this or it just refuses to play.
    $bundle['bc_entity_type'] = $entity_type;

    // Sneak more meta attributes in, We want to be like RDF.
    // This value MAY be saved against the content type
    // if you also have rdf_mapping.module enabled.
    $bundle['rdf_mapping'] = array(
      'rdftype' => array($curie),
    );

    // dpm(array($class_def, $bundle), $class_id);

    $data['bundles'][$bundle_id] = (object) $bundle;
  }
  // Now have a list of content types to import.

  // A formal RDFs output would state that all the properties apply to
  // the actual Class, eg here:
  //
  // schema:alternateName a rdfs:Property;
  //  rdfs:label "alternateName"@en;
  //  schema:rangeIncludes ns1:Text;
  //  rdfs:comment "An alias for the item."@en .
  //
  // the item should ALSO state:
  //  rdfs:domain schema:Thing;
  // but schema.org fails to do that.
  // Therefore, if there is only one Class in the dataset,
  // we must assume that all properties apply to that class.

  $default_class_id = NULL;
  if (count($data['bundles']) == 1) {
    $bundle_ids = array_keys($data['bundles']);
    $default_class_id = reset($bundle_ids);
  }

  // Now the fields (instances).
  // It's the Drupal fields that are tricky, all the richness goes in here.

  $properties = array();
  // GOTCHA, we must look for both rdf and rdfs One with s, one without!
  // FOAF said without, schema.org said with. FFS.
  if (isset($index_by_type['rdfs:Property'])) {
    $properties = $index_by_type['rdfs:Property'];
  }
  elseif (isset($index_by_type['rdf:Property'])) {
    // Pretty sure this is wrong, but it's in use.
    $properties = $index_by_type['rdf:Property'];
  }

  // dpm($properties);
  $data['fields'] = array();
  foreach ((array) $properties as $property_id => $property_def) {
    // The properties listed in RDF sets are often very loosly coupled to their
    // Class. Maybe attached to multiple, none, or may assume the current
    // context. Lets figure them out.

    list($field, $instances) = rdfs2cck_property_to_cck_field($property_def, $property_id, $default_class_id, $parser);

    $data['fields'][$field['field_name']] = $field;

    foreach ($instances as $instance) {
      $field_bundle_id = $instance['bundle'];
      $data['instances'][$field['field_name']][$field_bundle_id] = $instance;

      // Pull the RDF Mapping data up from the field and onto the bundle, because
      // that's where rdf.module may expect it to be.
      $data['bundles'][$field_bundle_id]->rdf_mapping[$instance['field_name']] = $instance['rdf_mapping'];
    }

    // The schema definitions that inherit from superclasses *could*
    // sort these inherited fields into Drupal 'fieldgroups' for a UI more like
    // Freebase, but that meta-info seems rare in the wild so far.
    // see freebase_api if we want to pull this functionality back in.

  }
  // Done preparing each field and instance.

dpm($data);
  return $data;
}


/**
 * Transform a RDFs 'property' definition into a bundle_copy style field def.
 *
 * Note that bundle_copy treats fields and field instances separately.
 * However, all other schemas provide field definition info together,
 * so we have to build the field and instance at the same time.
 *
 * Most of the domain-specific logic is here.
 *
 * As well as the default properties, and the ones that I've discovered useful
 * for certain field types (eg an image field required default_image to be set)
 * I also add a fields 'rdf_mapping' info about the predicate being used here.
 * This needs to be copied up into the $bundle-rdf_mapping if it is to be
 * saved, as field_api doesn't deal with rdf predicates.
 *
 * @param array $property_def
 *   Property definition from the schema array.
 *   Probably just a couple of statements giving the field a label and
 *   hopefully a type.
 * @param string $property_id
 *   Property ID.
 * @param string $default_class_id
 *   Name of the bundle to attach this field to, if the field_def does not
 *   set it explicitly itself.
 * @param $parser
 *   Handle on the parser object to do CURIE resolution etc.
 *
 * @return array
 *   ($field, $instances)
 *   A field definition AND a list of field instances.
 *   NULL if invalid or unwanted.
 */
function rdfs2cck_property_to_cck_field($property_def, $property_id, $default_class_id, $parser) {
  $curie = $parser->getPname($property_id);
  list($ns, $id) = $parser->splitURI($curie);

  $field_id = rdfs2cck_machine_name($id);
  $label = strip_tags(rdfs2cck_fetch_property($property_def, 'rdfs:label', TRUE));
  if (empty($label)) {
    $label = $id;
  }

  // What type of field is this to be?
  // Defaults to string if not otherwise specified.
  $expected_type = rdfs2cck_fetch_property($property_def, 'rdfs:range', TRUE);

  // START BUILDING the field definitions.
  // We need to create both the base field def and the field instance(s)
  // simultaneously.
  $field = field_schema_importer_field_defaults($field_id);

  $bundle_ids = rdfs2cck_fetch_property($property_def, 'rdfs:domain', FALSE);
  if (empty($bundle_ids) && $default_class_id) {
    $bundle_ids = array($default_class_id);
  }

  // Create a number of field instances.
  // USUALLY only one, but schemas are odd, so assume we will loop.
  $instances = array();
  foreach ($bundle_ids as $bundle_id) {
    $instance = field_schema_importer_field_instance_defaults($field_id, $bundle_id);
    $instance['label'] = $label;

    $instance += array(
      // Sneak more meta attributes in.
      // These are not cck proper, but come along for the ride
      // for the benefit of other tools.
      'rdf_mapping'   => array(
        'predicates' => array($curie),
        'datatype'   => $expected_type,
      ),
      'expected_type' => $expected_type,
    );

    // I've not seen a schema which defines this, assume single for now.
    $field['cardinality'] = 1;

    // Some other required properties will be filled in during validation.

    $node_types = node_type_get_types();
    // Set the *expected* referenceable_type as a hint to the user,
    // even if we can't always use it.
    // This is a field attribute of our own, not part of Drupal Field API.
    if ($expected_type) {
      // $expected_type is a CURIE. We want a machine name and an URL from it.
      list($e_ns, $e_id) = $parser->splitURI($expected_type);
      $e_type_id = rdfs2cck_machine_name($e_id);
      $field['referenceable_types'] = array(
        $e_type_id => $parser->expandPName($expected_type),
      );
    }
    // Knowing the expected type comes in handy when setting up
    // entityreferencee.

    // Deduce and set the field type.
    // This comes from the 'range' that should be set on a property.
    //
    // In the wild, (schema.org) almost everything is just 'text'.
    // In the less wild, many properties refer to other objects,
    // eg foaf:Person->address is an 'address' struct.
    // Occasionally we will detect integers or datetimes, or just owl:Thing.
    //
    // Fill these is as best guess as we go.

    // If we do not have enough widgets turned on (for date, or filefield)
    // the suggestion here will still be for the *desired* type we want to see,
    // but before continuing, the UI validator may replace all the unavailable
    // types with "text".

    // Further module and widget default settings are required later on,
    // but will be filled in automatically by the validation process.

    switch ($expected_type) {

      case '/type/datetime':
        $field['type'] = 'datetime';
        $instance['widget']['type'] = 'date_select';
        break;

      case '/common/webpage':
        $field['multiple'] = FALSE;
        $field['type'] = 'link_field';
        $instance['widget']['type'] = 'link';
        break;

      case '/common/weblink':
        $field['type'] = 'link_field';
        $instance['widget']['type'] = 'link';
        break;

      case '/common/image':
        $field['type'] = 'image';
        $field['module'] = 'image';
        $instance['widget']['type'] = 'image_image';
        $instance['widget']['module'] = 'image';
        $instance['settings']['file_extensions'] = 'png gif jpg jpeg';
        $instance['default_image'] = NULL;
        break;

      case '/type/float':
        $field['type'] = 'number_decimal';
        $instance['widget']['type'] = 'number';
        break;

      case '/type/int':
        $field['type'] = 'number_integer';
        $instance['widget']['type'] = 'number';
        break;

      case '/type/text':
        $field['type'] = 'text';
        $instance['widget']['type'] = 'text_textfield';
        break;

      case '/type/enumeration':
        $field['type'] = 'text';
        $instance['widget']['type'] = 'optionwidgets_select';
        break;

      default:
        // References to other value types are PROBABLY expected to become
        // references to other data objects. That's great, but may get hard.
        if (!@empty($node_types[$referenceable_type])) {
          // YAY. An appropriate target content type exists already.
          $field['type'] = 'entityreference';
          $instance['widget']['type'] = 'entityreference_autocomplete';
        }
        else {
          // Default to text, and let them sort it out later
          // They can reset this to a nodereference to one of their custom
          // content types if they wish.
          $field['type'] = 'text';
          $instance['widget']['type'] = 'text_textfield';
        }
        break;
    }

    // Also, tweak some known issues out,
    // turning these boring properties off by default.
    switch ($property_id) {
      case 'rdfs:isDefinedBy':
      case 'vs:status':
        $instance['create'] = FALSE;
        break;
    }

    $instances[$bundle_id . '=' . $field_id] = $instance;
  }
  dpm(array($property_def, $field));
  return array($field, $instances);
}

/**
 * Sort the given parser result subjects into type.
 *
 * eg rdfs:Class (or foaf:Person)
 *
 * Can't use Sparql queries on in-memory triple collections it seems,
 * so just chug through the array as PHP.
 *
 * If the optional filter_by_type parameter is given, only the named type
 * results will be returned.
 *
 * Statements about subjects with no explicit type are dropped.
 *
 * A subject may be of more than one type,
 * and will appear in more than one list if so.
 *
 * @param ARC2_RDFParser $parser
 *   The current dataset.
 *
 * @param string $filter_by_type
 *   Optional type to filter results by.
 *
 * @returns array
 *   nested list of items, as would be returned by getSimpleIndex,
 *   but with type as a primary key.
 */
function rdfs2cck_query_items_by_type(ARC2_RDFParser $parser, $filter_by_type = NULL) {
  $found = array();
  $triples = $parser->getTriples();
  // True here means the data is flattened and easier to work with.
  $index = ARC2::getSimpleIndex($triples, TRUE);

  foreach ($index as $subject_id => &$subject) {

    // Sometimes ARC2 processNode function in RdfaExtractor
    // hard codes the typeof to the full URL
    // 'p' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
    // not the short CURIE. 'rdf:type'
    // Check for both.
    $type_keys = array('rdf:type', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type');
    foreach ($type_keys as $type_key) {
      if (isset($subject[$type_key])) {
        $types = $subject[$type_key];
        foreach ($types as $type) {
          // Use short names as often as possible.
          $type_name = $parser->getPName($type);
          $found[$type_name][$subject_id] = $subject;
        }
      }
    };
  }

  // Note that the primary identifier of subjects extracted here is
  // likely to be the full URI.
  // eg http://schema.org/Organization
  // Or it may come out as a temporary CURIE ns0:Organization
  // This is technically correct.
  // We probably prefer the nice named CURIEs though, so we should
  // work on keying them as
  // schema:Organization
  // soon.

  if (!empty($filter_by_type)) {
    return isset($found[$filter_by_type]) ? $found[$filter_by_type] : array();
  }
  return $found;
}

/**
 * Allow for certain vocabularies to use different predicates for properties.
 *
 * Use this when fetching values from an ARC2 item, so you don't have to
 * worry about that.
 *
 * This will allow variious aliases for some properties, sort out the nested
 * arrays and also avoid duplicates that could be introduced if a datasource
 * is using mixed vocabularies..
 *
 * @param $item
 * A 'Subject' data array, as from an ARC2 index.
 * @param string $predicate
 * eg rdf:label.
 * @param bool $single
 * Most of the way through, all statements allow for multiple values on the
 * same predicate. But if you finally want the raw value, set this.
 *
 * @return array|mixed
 * The values of properties asked for, even if they were stored by other names.
 */
function rdfs2cck_fetch_property(array $item, $predicate, $single = FALSE) {
  // Add to this mapping array to extend additional meanings.
  $mappings = array(
    'rdfs:label' => array(
      'rdfs:label',
    ),
    'rdfs:comment' => array(
      'rdfs:comment',
    ),
  );
  if (isset($mappings[$predicate])) {
    $mapping = $mappings[$predicate];
    if (!in_array($predicate, $mappings)) {
      $mappings[] = $predicate;
    }
  }
  else {
    // No aliases, no mapping, just look for exactly this property.
    $mapping = array($predicate);
  }

  $result = array();
  foreach ($mapping as $alias) {
    if (isset($item[$alias])) {
      // This is still an array here.
      foreach ($item[$alias] as $value) {
        // Aliases can introduce double-ups. Avoid that.
        if (!in_array($value, $result)) {
          $result[] = $value;
        }
      }
    }
  }
  if ($single) {
    return reset($result);
  }
  return $result;
}


/**
 * Find and include the required ARC2 library.
 *
 * Utility
 */
function _rdfs2cck_load_arc() {
  if (class_exists('ARC2')) {
    return TRUE;
  }

  // Possible paths to the ARC2 PHP library.
  $search_paths = array(
    'sites/all/libraries/arc',
    'sites/all/libraries/ARC2/arc',
    drupal_get_path('module', 'rdfx') . '/vendor/arc',
  );
  if (module_exists('libraries')) {
    $search_paths[] = libraries_get_path('ARC2') . '/arc';
  }
  $rdf_arc2_path = '';
  foreach ($search_paths as $rdf_arc2_path) {
    if (file_exists($rdf_arc2_path . '/ARC2.php')) {
      @include_once $rdf_arc2_path . '/ARC2.php';
      return TRUE;
    }
  }
  drupal_set_message(t("No ARC library found at %rdf_arc2_path", array('%rdf_arc2_path' => $rdf_arc2_path)));
  return FALSE;
}

/**
 * Create Drupal machine-names from camelcase or CURIE element names.
 *
 * When working with cck, Field names are limited to 32 characters.
 * This MUST be kept to or things like tablenames break.
 *
 * @param string $name
 *   CURIE style identifier.
 *
 * @return string
 *   Appropriate to be used as a fieldname.
 */
function rdfs2cck_machine_name($name) {
  while (strlen($name) > 32 && strstr($name, '/')) {
    // Slice leftmost fragments off until it's short enough.
    $name = preg_replace('|^[^/]*/|', '', $name);
  }
  $name = str_replace('/', '_', ltrim($name, '/'));
  $name = strtolower(preg_replace('/([^A-Z])([A-Z])/', "$1_$2", $name));
  return $name;
}
