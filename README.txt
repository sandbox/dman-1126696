CCK Importer
============

This utility module helps you import a Content Type Definition (Node Type)
from a number of sources. 
It provides a UI for schema selection, and an extended content type all-in-one
edit form for reviewing and tuning the imported schema before content type 
creation.

Schema sources are provided as hooks, so schemas of various types may be 
translated to the CCK-native version.
@see HOOK_cck_importer_services()

@author Dan Morrison (dman) http://coders.co.nz
@version 2010

Theory
-------------------------------------------------------------------------------
A Content Type closely resembles an Object-Oriented 'Class' with its typed
properties, and can be represented with a 'schema' in certain syntaxes or
languages, or a "Class Diagram" in UML.

This natural fit has been extended to allow Drupal Admins to import Classes
from external sources.

An external data type schema may define such things as 
There is a type of thing called a "Book"
It has properties such as:
Title [string]
Author (possibly multiple) [string or reference]
ISBN number [string]
Publisher [string or reference]
... etc

This naturally can be modeled directly in CCK. But wouldn't it be nice if
someone had already done the field creation for you? And even better if 
everyone used the same field names?
(not 'ISBN', 'ISBN number', 'isbn_no', 'ISB Number')
(Yes the last one more is correct. No, it's not clever)

So lets get these schemas out in the open and re-use them!

Syntax - CCK
-------------------------------------------------------------------------------
The base type of supported syntax is the native Drupal '$content' content
type definition array published, and used internally by the 
"Content Creation Kit". Support for import and export of this is built in to 
CCK (Content Copy module) but it is helpful to be able to load this code
directly from a file or URL, not just with copy & paste of PHP code.

UI
-------------------------------------------------------------------------------
CCK Importer provides a UI for this, allowing you to specify either
- a local filepath
- a remote URL
- upload from your desktop
as a source to load a file containing your CCK $content export from.
More can be added through hooks (hook_cck_importer_services)

Alternative Syntaxes
-------------------------------------------------------------------------------
CCK Importer publishes a few Drupal hooks that allow other modules to extend
and provide extra syntaxes, as well as extra data sources or lookup services.

The first of these is *XSD* Schema, which translates the content model rules of
an XML data structure into a data type with a similar structure within Drupal.
When that is working correctly, you can create a Drupal Content Type with fields
that directly match an import (or export) of data supplied in XML format.

Another example is the *Freebase* CCK schema importer, which allows you to 
choose a Freebase date type (eg, 'book', or 'person') and thus set up a Drupal 
Content type that maps directly to Freebase data, field-for-field.

This is provided as part of the Freebase API project