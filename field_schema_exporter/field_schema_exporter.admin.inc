<?php
/**
 * @file
 * UI page listing exportable schemas
 */

/**
 * List all bundles and link to their schema export URLs.
 */
function field_schema_exporter_admin() {
  $out = array();

  $out['format_heading'] = array(
    '#type' => 'html_tag',
    '#tag' => 'h2',
    '#value' => t('Supported formats'),
  );

  $formats = field_schema_exporter_get_formats();
  $format_descriptions = array();
  foreach ($formats as $format_id => $format) {
    $format_descriptions[$format_id] = array(
      'name' => array(
        '#type' => 'html_tag',
        '#tag' => 'dt',
        '#value' => $format['name'],
      ),
      'description' => array(
        '#type' => 'html_tag',
        '#tag' => 'dd',
        '#value' => $format['description'],
      ),
    );
  }
  // html_tag rendering is weak. Render a little by hand.
  // https://api.drupal.org/comment/44333#comment-44333
  $out['format_descriptions'] = array(
    '#type' => 'html_tag',
    '#tag' => 'dl',
    '#value' => drupal_render($format_descriptions),
  );

  // Build a table listing bundles and export links.
  $bundle_description_header = array('Entity', 'Bundle', 'Export');
  $all_bundles = field_info_bundles();
  $bundle_description_rows = array();
  foreach ($all_bundles as $entity_type => $entity_bundles) {
    $row = array();
    // Add the bundle type as the first cell with rowspan.
    $row['entity'] = array(
      'rowspan' => count($entity_bundles),
      'data' => $entity_type,
    );
    // List each bundle.
    foreach ($entity_bundles as $bundle_id => $bundle) {
      // TODO there seems to be useful 'admin' link detail available here,
      // I could link to that, but how to extract it?
      $row['label'] = $bundle['label'];

      // Prepare the export links. Overthemed...
      $export_links = array();
      foreach ($formats as $format_id => $format) {
        $export_url = field_schema_exporter_export_url($entity_type, $bundle_id, $format_id);
        if ($export_url) {
          $export_links[$format_id] = array(
            'title' => $format['name'],
            'href' => $export_url,
          );
        }
      }
      $rendered_links = theme('links', array('links' => $export_links));
      $row['export']['data'] = $rendered_links;

      $bundle_description_rows[$entity_type . $bundle_id] = $row;
      $row = array();
    }
  }

  $out['bundle_descriptions'] = array(
    '#theme' => 'table',
    '#header' => $bundle_description_header,
    '#rows' => $bundle_description_rows,
  );

  return $out;
}
