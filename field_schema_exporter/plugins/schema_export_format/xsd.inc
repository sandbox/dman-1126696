<?php
/**
 * @file
 * Provides XSD format for schema exporting.
 */

$plugin = array(
  'name' => 'XSD',
  'description' => 'XML Schema Document, as used by SOAP and other data interchange mechanisms. Quite complex, but supports nested complexTypes and things like field cardinality.',
  // Name the export callback.
  'build' => 'field_schema_exporter_xsd_build',
  'export' => 'field_schema_exporter_xsd_export',
);

/**
 * Build and serve the XSD with headers.
 */
function field_schema_exporter_xsd_export($entity_type, $bundle_name) {
  $schema = _field_schema_exporter_xsd_create_doc();
  field_schema_exporter_xsd_build($schema, $entity_type, $bundle_name);
  $content = $schema->ownerDocument->saveXML();
  drupal_add_http_header('Content-Type', 'application/xml');
  print $content;
  drupal_exit();
}

/**
 * Build element structure representing the given entity.
 *
 * This attaches the result to a suitably prepared DOMElement container.
 * Call this to add several entities to one doc if needed.
 */
function field_schema_exporter_xsd_build($schema, $entity_type, $bundle_name) {
  $xsd_ns = 'http://www.w3.org/2001/XMLSchema';
  $rdfs_ns = 'http://www.w3.org/2000/01/rdf-schema#';
  if (empty($schema)) {
    $schema = _field_schema_exporter_xsd_create_doc();
  }
  $dom = $schema->ownerDocument;

  // Fetch field and property info.
  $entity_property_info = entity_get_property_info($entity_type);
  // $entity_property_info sorta has field info in there as well,
  $bundle_property_info = $entity_property_info['bundles'][$bundle_name]['properties'];
  // But it's not as complete as:
  $fields_info = field_info_instances($entity_type, $bundle_name);

  // Build element representing entity.
  // <xs:element name="employee">
  // ..<xs:complexType>
  // ....<xs:sequence>
  // ......<xs:element name="firstname" type="xs:string"/>
  // ......<xs:element name="lastname" type="xs:string"/>
  // ....</xs:sequence>
  // ..</xs:complexType>
  // </xs:element>
  $element = $dom->createElementNS($xsd_ns, 'element');
  $schema->appendChild($element);

  // HINT: To keep the extra namespace declarations under control,
  // always add the new element to the container ASAP. Before adding more.
  #$element->setAttributeNS($xsd_ns, 'name', $bundle_name);
  $element->setAttribute('name', $bundle_name);

  $complextype = $dom->createElementNS($xsd_ns, 'complexType');
  $element->appendChild($complextype);
  // Dunno why all examples put fields in a 'sequence', can't find examples
  // where order is not important. But OK.
  $fieldset = $dom->createElementNS($xsd_ns, 'sequence');
  $complextype->appendChild($fieldset);

  // Start by adding the UNIQUE bundle fields on an entity.
  // Boring built-in ones can come later, or through inheritance.
  foreach ($bundle_property_info as $field_id => $field_info) {
    _field_schema_exporter_xsd_add_element($fieldset, $field_id, $field_info);
  }

  // Build the inherited enitity def separately for now.
  $entity_element = $dom->createElementNS($xsd_ns, 'element');
  $schema->appendChild($entity_element);
  #$entity_element->setAttributeNS($xsd_ns, 'name', $entity_type);
  $entity_element->setAttribute('name', $entity_type);
  $complextype = $dom->createElementNS($xsd_ns, 'complexType');
  $entity_element->appendChild($complextype);
  $fieldset = $dom->createElementNS($xsd_ns, 'sequence');
  $complextype->appendChild($fieldset);

  $fields = $entity_property_info['properties'];
  foreach ($fields as $field_id => $field_info) {
    _field_schema_exporter_xsd_add_element($fieldset, $field_id, $field_info);
  }

  return $element;
}


/**
 * Add a representation of each known field type as an XSD element.
 *
 * ... or equivalent complexType or simpleType.
 *
 * @param DOMElement $container
 *   Context element.
 * @param string $field_id
 *   Field ID
 * @param array $field_info
 *   Field info as from from entity_get_property_info()
 *
 * @return DomNode
 *   The created element (or complexType).
 */
function _field_schema_exporter_xsd_add_element(DOMElement $container, $field_id, $field_info) {
  $xsd_ns = 'http://www.w3.org/2001/XMLSchema';
  $rdfs_ns = 'http://www.w3.org/2000/01/rdf-schema#';
  $dom = $container->ownerDocument;

  $field_base_info = field_info_field($field_id);

  if (empty($field_info['type'])) {
    // Unknown type.
    // 'title' for instance has no 'type' from entity_get_property_info().
    // Assume simple text.
    $field_info['type'] = 'text';
  }

  $type = $field_info['type'];
  // Field_info stores lists funny.

  $element = $dom->createElementNS($xsd_ns, 'element');
  $container->appendChild($element);

  // Add common attributes and tags.

  // Every field must have a name.
  $element->setAttribute('name', $field_id);

  // Extend the element with our own label tag in custom appinfo.
  if (!empty($field_info['label'])) {
    _field_schema_exporter_xsd_add_appinfo($element, 'label', $rdfs_ns, $field_info['label']);
  }
  // XSD doesn't have a description field, use the documentation tag.
  if (!empty($field_info['description'])) {
    _field_schema_exporter_xsd_add_documentation($element, $field_info['description']);
  }

  // We are going to have to abstract this big choice out somehow.

  switch ($type) {

    case 'text':
      $element->setAttribute('type', 'xsd:string');
      break;

    case 'token':
      $element->setAttribute('type', 'xsd:string');
      break;

    case 'uri':
      $element->setAttribute('type', 'xsd:anyURI');
      break;

    case 'date':
      $element->setAttribute('type', 'xsd:dateTime');
      break;

    case 'boolean':
    case 'integer':
      // These ones are named the same in both syntaxes.
      $element->setAttribute('type', 'xsd:' . $type);
      break;

    default:
      // Look for a magic function that handles the named field.
      // Add these as needed.
      // Most useful for additionally complexTypes that need nesting.
      $func_name = '_field_schema_exporter_xsd_add_' . $type;
      if (function_exists($func_name)) {
        $element = $func_name($element, $field_id, $field_info);
      }
      else {
        // Default to text. Needs to be something.
        $element->setAttribute('type', 'xsd:string');
        // Known types here are 'user' and 'node'.
      }

  }


  return $element;
}

/**
 * Magically named function for fields of type 'text_formatted'.
 */
function _field_schema_exporter_xsd_add_text_formatted(DOMElement $element, $field_id, $field_info) {
  // Eg body text with format and summary.

  $xsd_ns = 'http://www.w3.org/2001/XMLSchema';
  $dom = $element->ownerDocument;

  $complextype = $dom->createElementNS($xsd_ns, 'complexType');
  $element->appendChild($complextype);
  $fieldset = $dom->createElementNS($xsd_ns, 'sequence');
  $complextype->appendChild($fieldset);
  foreach ($field_info['property info'] as $property_id => $property_info) {
    $property = _field_schema_exporter_xsd_add_element($fieldset, $property_id, $property_info);
  }

  return $element;
}

/**
 * Set up an XML Schema document with heders and things.
 *
 * @return DOMElement
 *   New schema element in a new DOMDocument.
 */
function _field_schema_exporter_xsd_create_doc() {
  $xsd_ns = 'http://www.w3.org/2001/XMLSchema';
  $rdfs_ns = 'http://www.w3.org/2000/01/rdf-schema#';

  $dom = new DOMDocument('1.0');
  $dom->formatOutput = TRUE;

  // Build doc header
  // <?xml version="1.0"
  // <xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  // ..<xsd:annotation>
  // ....<xsd:documentation>
  $schema = $dom->createElementNS($xsd_ns, 'xsd:schema');
  $dom->appendChild($schema);
  // Need additional namespaces? Need to set the prefix.
  $schema->setAttributeNS('http://www.w3.org/2000/xmlns/', "xmlns:xsd", $xsd_ns);
  $schema->setAttributeNS('http://www.w3.org/2000/xmlns/', "xmlns:rdfs", $rdfs_ns);

  $documentation_text = "Schema exported by Drupal field_schema_exporter, https://www.drupal.org/node/1126696";
  _field_schema_exporter_xsd_add_documentation($schema, $documentation_text);
  $documentation_text = "Extended with rdfs:label in the appinfo to provide a human-readable label for the field.";
  _field_schema_exporter_xsd_add_documentation($schema, $documentation_text);

  return $schema;
}

/**
 * Insert an annotation documentation tag.
 *
 * @param DOMElement $container
 *   Context element.
 * @param string $text
 *   Documentation string.
 *
 * @return DOMNode
 *   The added element.
 */
function _field_schema_exporter_xsd_add_documentation(DOMElement $container, $text) {
  $xsd_ns = 'http://www.w3.org/2001/XMLSchema';
  $dom = $container->ownerDocument;
  // Check for an existing annotation container.
  $annotation = _field_schema_exporter_xsd_getChildByTagName($container, 'annotation');
  if (!$annotation) {
    $annotation = $dom->createElementNS($xsd_ns, 'annotation');
    $container->appendChild($annotation);
  }
  return $annotation->appendChild($dom->createElementNS($xsd_ns, 'documentation', $text));
}

/**
 * Add a (custom) tag to the appinfo container in the annotations.
 *
 * Will extend existing annotations if they are already set.
 *
 * @param DOMElement $container
 *   Content element.
 * @param string $tag
 *   Tag name to add inside the appinfo.
 * @param string $tag_ns
 *   Namespace for the tag name to add inside the appinfo.
 * @param string $text
 *   Content of the custom tag.
 *
 * @return DomNode
 *   The created tag.
 */
function _field_schema_exporter_xsd_add_appinfo(DOMElement $container, $tag, $tag_ns, $text) {
  $xsd_ns = 'http://www.w3.org/2001/XMLSchema';
  $dom = $container->ownerDocument;
  // Check for an existing annotation container.
  $annotation = _field_schema_exporter_xsd_getChildByTagName($container, 'annotation');
  if (!$annotation) {
    $annotation = $dom->createElementNS($xsd_ns, 'annotation');
    $container->appendChild($annotation);
  }

  // Check for an existing appinfo container.
  $appinfo = _field_schema_exporter_xsd_getChildByTagName($annotation, 'appinfo');
  if (! $appinfo) {
    $appinfo = $annotation->appendChild($dom->createElementNS($xsd_ns, 'appinfo'));
  }

  return $appinfo->appendChild($dom->createElementNS($tag_ns, $tag, $text));
}

/**
 * Get child of the current tags named $name.
 *
 * GetElementsByTagName can't be told to just fetch immediate children.
 *
 * TODO this skips namespaces because namespaces are getting really tedious.
 *
 * @ingroup Utility
 *
 * @param $container
 * @param $name
 */
function _field_schema_exporter_xsd_getChildByTagName($container, $name) {
  for ($n = $container->firstChild; $n !== NULL; $n = $n->nextSibling) {
    if ($n instanceof DOMElement && $n->localName == $name) {
      return $n;
    }
  }
  return NULL;
}
