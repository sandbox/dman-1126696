<?php
/**
 * @file
 * Return a bundles schema in the requested format
 */

/**
 * Return the requested entity schema in the requested format.
 *
 * Most of these return raw text with their own MIME etc.
 *
 * Add debug at the end of the URL to get a screen-friendly version.
 * eg call /admin/structure/types/manage/page/export/xsd/debug
 */
function field_schema_exporter_page($entity_type, $bundle_name, $format_id, $debug = FALSE) {
  $out = array();
  $result = 'No result';
  $formats = field_schema_exporter_get_formats();
  if (!isset($formats[$format_id])) {
    drupal_set_message("Unsupported format. No schema_export_format %format_id. Perhaps the plugin is missing, or this is a typo.", array('%format_id' => $format_id));
    drupal_not_found();
    // Exits.
  }
  $format = $formats[$format_id];

  if ($debug) {
    // Invoked by adding any garbage to the URL.
    // Just build the result and show it on the page.
    $callback = $format['build'];
    $result = $callback(NULL, $entity_type, $bundle_name);
    if (is_a($result, 'DOMNode')) {
      $result = $result->ownerDocument->saveXML();
    }
    dpm(get_defined_vars());
    debug($result);
    return 'Debugging';
  }

  // Invoke the plugins render_callback function.
  if (function_exists($format['export'])) {
    $callback = $format['export'];
    $result = $callback($entity_type, $bundle_name);
    // Exits.
  }

  $out['result'] = array(
    '#markup' => $result,
  );
  return $out;
}
