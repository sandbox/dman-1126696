<?php
/**
 * @file
 * The meat of the field_schema_importer process.
 *
 * Create and modify functions.
 *
 * @author 'dman' Dan Morrison http://coders.co.nz/
 * @version 2009-10
 */

/**
 * Do our best to repair any missing bits in a $content object (by reference).
 *
 * Copied largely from content_copy_import_form_submit()
 *
 * This is NOT being done as a form validator as we want to validate and fix up
 * content type arrays at several stages in the process, not just form
 * submissions.
 *
 * @param Object $bundle
 *   Expected to look like a node_type_get_type() result.
 *
 * @return bool
 *   FALSE if this is impossible
 */
function field_schema_importer_validate_content_type(&$bundle) {
  $success = TRUE;

  // Ensure there is a good amount of data.
  if (empty($bundle)) {
    drupal_set_message("Tragically invalid, no content type data at all", 'error');
    return FALSE;
  }
  if (!is_object($bundle)) {
    drupal_set_message("Bundle is not an object", 'error');
    return FALSE;
  }
  if (empty($bundle->type)) {
    drupal_set_message("No type definition? This can't be right", 'error');
    return FALSE;
  }
  if (empty($bundle->name)) {
    drupal_set_message("Content type name is required", 'error');
    $success = FALSE;
  }

  // Make sure the imported content type doesn't already exist in the database.
  if (empty($bundle->orig_type)) {
    // If there is no old name, this is supposed to be a new type.
    // Check that is so.
    $node_types = node_type_get_types();
    if (isset($node_types[$bundle->type])) {
      drupal_set_message(t('
        The content type "%name" already exists in this database,
        yet it was submitted as new.
        (It is supposed to have orig_type update flag set if it already exists).
        Never mind, carrying on updating it anyway.
      ', array(
        '%name' => $bundle->name,
      )), 'warning');
    }
  }
  if ($success) {
    drupal_set_message('Validated: Content type looks good');
  }
  return $success;
}

/**
 * Prepares incoming field definitions.
 *
 * Check if the required modules for all fields we are about to make are
 * available.
 *
 * Fills in any required missing stuff.
 * Updates the given data by reference.
 *
 * @param array $fields
 *   List of base field defs.
 *
 * @param array $instances
 *   List of field instances.
 *
 * @return bool
 *   Success.
 */
function field_schema_importer_validate_fields(&$fields, &$instances) {
  if (empty($fields)) {
    drupal_set_message("No fields selected at all? Is this correct?", 'warning');
    return FALSE;
  }

  $success = TRUE;
  // Perform pre-import error trapping and raise warnings.
  $not_enabled = array();

  // Make sure that all the field and widget modules in the import
  // are enabled in this database, and that most defaults are set.
  $field_types = field_info_field_types();
  $modules = module_list();

  // Note the use of handles when iterating the array, as we wish to modify it.
  foreach ($instances as $field_name => &$instance_set) {
    foreach ($instance_set as $i => &$instance) {
      // Also a handle.
      $field = &$fields[$field_name];
      // For interest, see if the base field already exists.
      $prior_instance = field_read_instance($instance['entity_type'], $instance['field_name'], $instance['bundle']);
      $prior_field = field_read_field($field['field_name'], array('include_inactive' => TRUE));

      $strings = array(
        '%field_name' => $field_name,
        '%field_type' => $field['type'],
        '%prior_field_type' => $prior_field['type'],
        '%known_field_types' => implode(', ', array_keys($field_types)),
      );

      if ($prior_field && ($field['type'] != $prior_field['type'])) {
        // This cannot work.
        // MAYBE should quash the complaint if they are not creating it.
        drupal_set_message(t('
          The proposed field %field_name of type %field_type conflicts with the
          prior field with the same ID but of type %prior_field_type.
          Field types cannot be changed, you must delete the conflicting one
          first, or change the machine name.
        ', $strings), 'warning');
        $instance['create'] = FALSE;
      }

      // Ensure the name is safe and short.
      $field['field_name'] = field_schema_importer_machine_name($field['field_name']);

      // Set some more defaults, to deal with certain undefined notices later.
      if (empty($field['cardinality'])) {
        $field['cardinality'] = -1;
      }
      if (empty($instance['cardinality'])) {
        $instance['cardinality'] = -1;
      }
      // Set some trivial defaults that usually would be done when using the UI.
      // The requirements for the defaults are different for each field type.
      $defaults = field_schema_importer_defaults();
      $instance_defaults = (array) @$defaults['field_instances'][$field['type']];
      $instance = array_replace_recursive($instance, $instance_defaults);

      // Check the named field type is available. If not it may need to be
      // provided by a contrib module.
      if (!isset($field_types[$field['type']])) {
        drupal_set_message(t("
          Unknown field type %field_type for %field_name.
          It is not available in the list of known types (%known_field_types).
          You may need to locate or enable the required contrib module for it.
          If you do not, we will default to treating it just text.
          ", $strings), 'warning');
        $not_enabled[] = $field['type'];
      }
      else {
        // If the *type* has changed through the UI, we need to update the
        // $field['module'] else things get confused subtly.
        // Ensure the field module matches the module of the field type.
        $field_type_def = $field_types[$field['type']];
        $field['module'] = $field_type_def['module'];
      }

      // Check we have enough modules enabled to serve the known data types.
      // eg 'number' field may not have been switched on yet.
      if (empty($field['module']) || empty($instance['widget']['module'])) {
        // I will let undefined through for now.
        // Hope the system can match it up later.
        // $field['field_name'] $field['type'] has empty module parameter
        // Otherwise I have to keep track of the module->widget lookup myself.
        // No thanks.
      }
      else {
        if (!module_exists($field['module'])) {
          $not_enabled[] = $field['module'];
        }
        if (!module_exists($instance['widget']['module'])) {
          $not_enabled[] = $instance['widget']['module'];
        }
      }
      // May need to find a way to ensure that the display settings are
      // legal when the type has changed. defaulting to 'text' doesn't take us
      // far, and causes warnings later.
      // Or maybe default to hidden and leave everything up to the editor.
      module_load_include('inc', 'field_ui', 'field_ui.admin');
      $valid_formatters = field_ui_formatter_options($field['type']);
      $valid_formatters['hidden'] = 'hidden';
      foreach ($instance['display'] as $view_mode => &$view_mode_settings) {
        if (!in_array($view_mode_settings['type'], array_keys($valid_formatters))) {
          if (!empty($field['create'])) {
            // Only mention this out loud if we are in fact about to save it.
            drupal_set_message(t("
              Display formatter %display is unsupported for a %field_type .
              It will be hidden from %view_mode view mode for now,
              please adjust the content type field display to choose an
              appropriate field renderer yourself.
            ", array(
              '%display' => $view_mode_settings['type'],
              '%field_type' => $field['type'],
              '%view_mode' => $view_mode,
            )), 'notice');
          }
          $view_mode_settings['type'] = 'hidden';
        }
      }

      // Though the predicate is per-instance,
      // the data-type defines the base field.
      if (isset($instance['rdf_mapping'])) {
        $field['rdf_mapping']['datatype'] = $instance['rdf_mapping']['datatype'];
      }

      // Do some additional per-field-type data fixups.
      switch ($field['type']) {

        case 'entityreference':
          // Ensure that referenceable_types is set up.
          //
          // If the entityreference field instance defines an target entity_type
          // and bundle then that makes up the 'expected type'
          //
          // The raw content def array sets the reference targets deep in the
          // handler settings.
          // We need to expose that and work with it directly at a higher level
          // of the form. 'referenceable_types' is our version of that data.
          if (!empty($field['settings']['target_type'])) {
            foreach ($field['settings']['handler_settings']['target_bundles'] as $bundle_id) {
              $ref_type = $field['settings']['target_type'] . ':' . $bundle_id;
              $field['referenceable_types'][$ref_type] = $ref_type;
            }
          }
          if (empty($field['referenceable_types'])) {
            $field['referenceable_types'] = array();
            drupal_set_message("
              Field <b>{$instance['label']}</b> :
              A entityreference is being created with no target type.
              This is OK for building frameworks,
              but will place the system into a slightly unstable state until you
              edit the content type to save the entityreference target rule later.
            ", 'warning');
          }
          elseif (!is_array($field['referenceable_types'])) {
            $field['referenceable_types'] = array($field['referenceable_types'] => $field['referenceable_types']);
          }
          break;

      }

      // There is a whole lot of detail that I shouldn't have to manage here,
      // and it causes errors if you try to change field types when importing.
      // Field crud should take care of that automatically instead.
      // If it is present, discard it.
      unset($field['storage']);
      unset($field['foreign keys']);
      unset($field['indexes']);
      unset($field['columns']);
    }
    // Each instance per field.
  }
  // Each field_id.

  // If any required module is not enabled, set an error message and fail.
  if ($not_enabled) {
    drupal_set_message(t('
      The following fields need additional module support :
        <strong>%modules</strong>.
      The schema definition being parsed expects these data types to be
      supported by special field modules.
      This happens if advanced fields (like geolocation)
      are expected but you don\'t have that field module enabled yet.
      More detail is shown in the table below.
      If you don\'t care about those fields, you can ignore this or switch them
      to an alternative storage method (ie. just let it be stored as text).
      ', array(
        '%modules' => implode(', ', array_unique($not_enabled)),
      )
    ), 'warning');
    $success = FALSE;
  }

  if ($success) {
    drupal_set_message('Validated: Field types look good');
  }
  // dpm(get_defined_vars(), __FUNCTION__);

  return $success;
}

/**
 * Set up the external targets for fields if needed.
 *
 * Run before the full content_save.
 *
 * @param array $content_definitions
 *   contains bundles, fields (field_bases) and instances.
 */
function field_schema_importer_prepare_fields(&$content_definitions) {
  foreach ($content_definitions['fields'] as &$field) {
    switch ($field['type']) {
      case 'taxonomy_term_reference':
        field_schema_importer_assert_vocabulary_exists($field);
        break;

      case 'entityreference':
        field_schema_importer_assert_entity_exists($field);
        break;

    }
    // Clean up our own memo.
    unset($field['referenceable_types']);
  }
}


/**
 * Create an entire content/entity type.
 *
 * ... based on the fact that an entityreference field expects it.
 *
 * @param array $field
 *   Field definition.
 *
 * @return bool
 *   Success.
 */
function field_schema_importer_assert_entity_exists(&$field) {
  // May need to create a new entity bundle.
  // At least unpack the submitted referenceable_types into entity_type:bundle
  // and adjust the configs for this field base.

  // $referenceable_types is an array.
  // We need to know both entity_type and bundle, as they are conflated into
  // a single selectbox in the UI.
  foreach ($field['referenceable_types'] as $referenceable_type) {
    list($entity_type, $bundle_id) = explode(':', $referenceable_type);

    $entity_info = entity_get_info($entity_type);
    $bundle = isset($entity_info['bundles'][$bundle_id]) ? $entity_info['bundles'][$bundle_id] : NULL;

    if (empty($bundle) && $bundle_id == 'new') {
      // Make a new one as best we can. Need entity_api support here.
      $bundle_id = $field['expected_type'];
      $bundle = new stdClass();
      $bundle->type = $bundle_id;
      $bundle->name = $field['label'];
      $bundle->is_new = TRUE;
      $bundle->description = 'Autocreated to provide a target entity for field ' . $field['field_name'];

      // Dunno what these things mean, or if they relate just to nodes or to
      // everything, but it seems this will not show up unless I also set:

      // Active.
      $bundle->disabled = FALSE;
      // Cannot change machine name.
      $bundle->locked = TRUE;
      // User-created (so editable).
      $bundle->custom = 1;
      // Can't it figure this out itself?
      $bundle->base = $entity_type . '_content';
      // Can't it figure this out itself?
      $bundle->module = $entity_type;
      // Looks like my attempt to abstract nodes to entities
      // doesn't help at all.

      // Have I still got the expected type URI?
      // We know enough, from the expectations of the field,
      // to say what RDF type URI the new entity should be.
      if (!empty($field['rdf_mapping']['datatype'])) {
        $bundle->rdf_mapping['rdftype'] = $field['rdf_mapping']['datatype'];
      }

      $bundle_saved = field_schema_importer_create_bundle($entity_type, $bundle);
      $strings = array(
        '%bundle_id' => $bundle_id,
        '%entity_type' => $entity_type,
        '%field_name' => $field['field_name'],
      );

      // Well, that may have worked?
      if ($bundle_saved) {
        drupal_set_message(t('Autocreated a new bundle of type %entity_type called %bundle_id so that %field_name has something to point to', $strings));
      }
      else {
        drupal_set_message(t('Failed to autocreate a new bundle of type %entity_type called %bundle_id for %field_name to reference.', $strings));
      }
    }
    // Else it already exists, so we can probably continue.

    // New set the field settings to refer to this.
    $field['settings']['target_type'] = $entity_type;
    $field['settings']['handler_settings']['target_bundles'] = array($bundle_id => $bundle_id);
  }
}



/**
 * Validates or creates a vocabulary to reference.
 *
 * Before saving an entity type that uses a taxonomy_term_reference, we should
 * ensure the referred vocab is ready. When creating on-the-fly, this may mean
 * creating the Vocab first.
 *
 * @param array $field
 *   A field instance that refers to its expected vocab by machine
 *   name. if <new> then make it.
 *
 * @return object
 *   The new vocab. If needed, the $field is updated by reference.
 */
function field_schema_importer_assert_vocabulary_exists(&$field) {
  // I need access to both the field instance and the field_base to set this up.
  // Label comes from instance, target entity is only in the field_base.

  $vocab_id = $field['vocabularies'];

  if ($vocab_id == 'new') {
    // Need to make it.
    $vocabulary = (object) array(
      'name' => $field['label'],
      'machine_name' => $field['expected_type'],
    );
    taxonomy_vocabulary_save($vocabulary);
    drupal_set_message(t('
      Created a new vocabulary !vocabulary
    ', array('!vocabulary' => l($vocabulary->name, "admin/structure/taxonomy/{$vocabulary->machine_name}/edit"))));
    $vocab_id = $vocabulary->machine_name;
    unset($field['vocabularies']);
    // $field['referenceable_types'][$i] = $vocabulary->machine_name;
    // Need to flush some vocab cache if I want to use it immediately it seems.
  }

  if (empty($vocab_id)) {
    drupal_set_message(t('
      The target vocabulary for %field_name is left undefined.
      You should edit the field settings later to assign it to your desired vocab.',
      array('%field_name' => $field['field_name'])
    ), 'notice');
  }
  elseif (isset($vocabulary) || ($vocabulary = taxonomy_vocabulary_load($vocab_id))) {
    // Vocab target is valid.
    // Attach the field.
    $field['settings']['allowed_values'][0] = array(
      'vocabulary' => $vocabulary->machine_name,
      'parent' => 0,
    );
  }
  else {
    drupal_set_message(t('
      Had difficulty finding a vocabulary with the ID "%vocab_id".
      Not sure how that could have happened. (maybe caching issue when creating a vocab on the fly)',
      array(
        '%vocab_id' => $vocab_id,
      )
    ), 'error');
  }

}


/**
 * Extra support for entityreference type fields.
 *
 * Check for the existence of content types that the field thinks it can link
 * to. Make it if needed.
 *
 * As we allow the creation of entityreference links to content types that don't
 * exist yet, we need to make a dummy content type to link to.
 * This content type can be imported later in the same way, but for now will be
 * mostly empty.
 *
 * The new content type will be based on the information provided by the
 * expectations of the entityreference field. These expectations MAY include an
 * rdf identifier that would be useful when filling in the fields from published
 * schemas later.
 * @see rdf_mapping.module
 */
function REMAKE_field_schema_importer_prepare_entityreference(&$field) {
  if (empty($field['referenceable_types'])) {
    // Not recommended, but you've been warned. Just carry on.
    return;
  }
  // The validate process cast our referenceable_types value into an array.
  $referenceable_type = reset($field['referenceable_types']);

  // If the referenceable type already exists, we are OK.
  $existing_types = node_type_get_types();
  if (isset($existing_types[$referenceable_type])) {
    // Say no more.
    return;
  }

  $new_type = array();

  if ($referenceable_type == 'new') {
    // Use the suggested expected_type  as a content type machine name,
    // so other imports will recognize it. Otherwise use the field_name.
    $new_type['type'] = field_schema_importer_machine_name($field['field_name']);
    $new_type['name'] = $field['label'];

    if (isset($field['expected_type'])) {

      // Stop and think!
      // The expected_type is not the identifier for the predicate,
      // it is the RANGE for the predicate.

      $new_type['type'] = field_schema_importer_machine_name($field['expected_type']);
      // Not sure if we should use the machine name or the field label here.
      $new_type['name'] = $field['expected_type'];
    }
  }
  else {
    // A referenceable_type is named, but doesn't exist.
    // Use the requested name when creating a new one.
    $new_type['type'] = field_schema_importer_machine_name($referenceable_type);
    $new_type['name'] = $referenceable_type;
  }

  // Make a new content type!
  $type_url_str = str_replace('_', '-', $new_type['type']);
  // $base_type_url_str = str_replace('_', '-', $field['type_name']);

  $strings = array(
    '!type_name' => $new_type['name'],
    '!type' => l($new_type['type'], 'admin/content/node-type/' . $type_url_str),
    '!field_name' => l($field['label'], 'admin/content/node-type/' . $type_url_str . '/fields/' . $field['field_name']),
    '!base_type' => l($field['type_name'], 'admin/content/node-type/' . $type_url_str),
  );
  drupal_set_message(t('
    Creating a new content type !type_name (!type)
    to refer to with a entityreference called !field_name from !base_type.
    ', $strings));

  // Need all the defaults added, to avoid warnings.
  $new_type += field_schema_importer_content_type_defaults($new_type['name'], $new_type['type']);
  $type_info = (object) $new_type;
  // We don't know a lot about this new type,
  // but we have some clues that are worth keeping.
  // The rdf_mapping specifically will be valuable if known.
  // Copy known values from the field definition
  // into the content type definition.

  // Take care, the rdf_mapping property of the FIELD - which is a relationship
  // is not the same as the rdf mapping of the TARGET - which is an object.
  // We are beyond the field now, and making an object definition.
  $type_info->rdf_rdftype = array();
  if (!empty($field['datatype'])) {
    $type_info->rdf_rdftype = $field['datatype'];
  }
  $default_rdf_map = reset($type_info->rdf_rdftype);

  $type_info->description = t('
    This content type was made to fulfil the expectations of the field !field_name.
    !default_rdf_map ', $strings + array('!default_rdf_map' => l($default_rdf_map, $default_rdf_map))
  );

  // Beware, this skips validation.
  node_type_save($type_info);
  content_clear_type_cache();
  drupal_set_message(t('
    Created a new content type !type_name (!type)
    to refer to with a entityreference called !field_name from !base_type.
    You may want to rename it to something friendlier.
    ', $strings));

  $field['referenceable_types'] = array($type_info->type => $type_info->type);
}

/**
 * Return a list of bundles I know how to create on the fly.
 *
 * Returns an array of entity-types, and info about their bundle_save
 * callback function.
 * This is really sparse, because most entities do not provide abstract bundle
 * creation, and those that do don't seem to have a published way of telling me.
 */
function field_schema_importer_createable_bundles() {
  return array(
    'node' => array(
      'bundle_save_callback' => 'node_type_save',
    ),
  );
  // Yeah. that's it.
}

/**
 * Create a new bundle within the given entity type.
 *
 * I looked hard, but I can't find a generic 'entity_bundle_create() function,
 * even within entityAPI, and even after investigating the
 * EntityController thing.
 *
 * Right now admittedly, the only actual bundle that is supported is 'node'
 *
 * @param string $entity_type
 *   Name.
 * @param object $bundle
 *   Bundle defintion.
 *
 * @return int
 *   SAVED_NEW or SAVED_UPDATED.
 *
 * @see node_type_save()
 */
function field_schema_importer_create_bundle($entity_type, $bundle) {
  $creatable = field_schema_importer_createable_bundles();
  if (!isset($creatable[$entity_type])) {
    return NULL;
  }

  $bundle_save_callback = $creatable[$entity_type]['bundle_save_callback'];
  if (!function_exists($bundle_save_callback)) {
    return NULL;
  }

  // Invoke (eg) node_type_save()
  $saved_status = $bundle_save_callback($bundle);
  drupal_set_message(t('Bundle "%bundle" has been saved.', array('%bundle' => $bundle->name)));
  return $saved_status;
}
