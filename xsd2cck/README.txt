XSD (Xml Schema) to CCK (Content Type definition) conversion
============================================================

A utility to take formal data specifications, provided as XSD Schemas, and
configure a Drupal instance with a Content Type appropriate for storing the
described data.

@author dman dan@coders.co.nz
 
Originally as part of the EOL Code Sprint
@see http://sprint.eol.org/


Rationale:
-------------------------------------------------------------------------------
CCK is, at the core, an object definition that may have many different
attributes (fields) of many different types (text,number,list) of data.

An XML document is often a serialization of a data object, which has many
different tags and values.
 
If importing from XML to Drupal CCK, we would like to preserve a 1:1 mapping
of the input data to the database storage. Good XML dialects provide a formal
DTD or XSD document that describes the shape of the data. We can use this
data definition to derive a content type definition!
 
Usage:
-------------------------------------------------------------------------------
This tool may be used stand-alone, to set up a site designed to manage such
XML within Drupal.
It may also (original intent) be used as a library invoked from other
special-purpose modules that need to create a specific and complicated
Content Type at install time.

One such module is 'cck_importer.module' which provides a fuller interface
that allows a user to tune the import result before creating the content type.

 
Approach:
-------------------------------------------------------------------------------
Drupal CCK provides an 'import' functionality, which takes raw PHP code (a
set of arrays) that become CCK content types and field definitions. This
interim code step is pretty hairy, but for now is a substitute for better
CRUD APIs.
 
The xsd2cck process analyzes a Schema document, and produces a PHP code
snippet - which is then fed to CCKs import function. It works with "CCK
Content Import". In the middle is an XSL Transformation document that reads
Schemas and writes PHP.
 
This is quite mad, but has the following benefits: - it works (within
limits) - it works within current constraints - we can deploy a published XSD
file as part of a module in plaintext
 
 
Caveats:
-------------------------------------------------------------------------------
Not all XSD features are supported. In fact, only a subset of Schema entity
definitions are understood yet. This may develop.
Not all XML has a schema. If there is no XSD or DTD, then you are out of
luck, or may have to develop your own.
if an XML dialect supplies a DTD - you can use the dtd2xsd.pl tool (see W3C)
and convert. I do not know how good a job it does, but certainly seems to
give it a good try.
 

XSD Support
-------------------------------------------------------------------------------
Currently supported subset of XSD, and its CCK mappings
(See the XSL document for more details)

Top level xsd:complexType
Top level xsd:element
 = Content type
 
 Expected to contain :
 @name = CCK Title
 @description = description
 @name (also becomes) CCK type
 
