<?php
$content_types = array();
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  PBCoreDescriptionDocument
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'PBCoreDescriptionDocument',
  'type' => 'pbcoredescriptiondocument',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '"The Master Container assembles together all collections
            of PBCore knowledge items. For PBCore these knowledge items are metadata descriptions of
            media. The MasterContainer is expressed as a document that hierarchically structures all
            the knowledge items and metadata terms and values related to a single data record
            associated with a media item. In our XML Schema Definition, the MasterContainer is
            referred to as the PBCoreDescriptionDocument."',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    
          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreIdentifier'.
// xsd:element 
$group = array (
  'label' => 'pbcoreIdentifier',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreidentifier',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreIdentifier'.
// xsd:element 

$field = array (
    'label' => 'pbcoreIdentifier',
    'field_name' => 'field_pbcoreidentifier',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreidentifier';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreTitle'.
// xsd:element 
$group = array (
  'label' => 'pbcoreTitle',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoretitle',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreTitle'.
// xsd:element 

$field = array (
    'label' => 'pbcoreTitle',
    'field_name' => 'field_pbcoretitle',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoretitle';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreSubject'.
// xsd:element 
$group = array (
  'label' => 'pbcoreSubject',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoresubject',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreSubject'.
// xsd:element 

$field = array (
    'label' => 'pbcoreSubject',
    'field_name' => 'field_pbcoresubject',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoresubject';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreDescription'.
// xsd:element 
$group = array (
  'label' => 'pbcoreDescription',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoredescription',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreDescription'.
// xsd:element 

$field = array (
    'label' => 'pbcoreDescription',
    'field_name' => 'field_pbcoredescription',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoredescription';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreGenre'.
// xsd:element 
$group = array (
  'label' => 'pbcoreGenre',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoregenre',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreGenre'.
// xsd:element 

$field = array (
    'label' => 'pbcoreGenre',
    'field_name' => 'field_pbcoregenre',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoregenre';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreRelation'.
// xsd:element 
$group = array (
  'label' => 'pbcoreRelation',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorerelation',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreRelation'.
// xsd:element 

$field = array (
    'label' => 'pbcoreRelation',
    'field_name' => 'field_pbcorerelation',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorerelation';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreCoverage'.
// xsd:element 
$group = array (
  'label' => 'pbcoreCoverage',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorecoverage',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreCoverage'.
// xsd:element 

$field = array (
    'label' => 'pbcoreCoverage',
    'field_name' => 'field_pbcorecoverage',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorecoverage';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreAudienceLevel'.
// xsd:element 
$group = array (
  'label' => 'pbcoreAudienceLevel',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreaudiencelevel',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreAudienceLevel'.
// xsd:element 

$field = array (
    'label' => 'pbcoreAudienceLevel',
    'field_name' => 'field_pbcoreaudiencelevel',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreaudiencelevel';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreAudienceRating'.
// xsd:element 
$group = array (
  'label' => 'pbcoreAudienceRating',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreaudiencerating',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreAudienceRating'.
// xsd:element 

$field = array (
    'label' => 'pbcoreAudienceRating',
    'field_name' => 'field_pbcoreaudiencerating',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreaudiencerating';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreCreator'.
// xsd:element 
$group = array (
  'label' => 'pbcoreCreator',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorecreator',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreCreator'.
// xsd:element 

$field = array (
    'label' => 'pbcoreCreator',
    'field_name' => 'field_pbcorecreator',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorecreator';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreContributor'.
// xsd:element 
$group = array (
  'label' => 'pbcoreContributor',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorecontributor',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreContributor'.
// xsd:element 

$field = array (
    'label' => 'pbcoreContributor',
    'field_name' => 'field_pbcorecontributor',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorecontributor';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcorePublisher'.
// xsd:element 
$group = array (
  'label' => 'pbcorePublisher',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorepublisher',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcorePublisher'.
// xsd:element 

$field = array (
    'label' => 'pbcorePublisher',
    'field_name' => 'field_pbcorepublisher',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorepublisher';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreRightsSummary'.
// xsd:element 
$group = array (
  'label' => 'pbcoreRightsSummary',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorerightssummary',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreRightsSummary'.
// xsd:element 

$field = array (
    'label' => 'pbcoreRightsSummary',
    'field_name' => 'field_pbcorerightssummary',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorerightssummary';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreInstantiation'.
// xsd:element 
$group = array (
  'label' => 'pbcoreInstantiation',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreinstantiation',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreInstantiation'.
// xsd:element 

$field = array (
    'label' => 'pbcoreInstantiation',
    'field_name' => 'field_pbcoreinstantiation',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreinstantiation';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreExtension'.
// xsd:element 
$group = array (
  'label' => 'pbcoreExtension',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreextension',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreExtension'.
// xsd:element 

$field = array (
    'label' => 'pbcoreExtension',
    'field_name' => 'field_pbcoreextension',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreextension';
  


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  ///////////////////////////////////
  // CONTENT TYPE:  pbcore.string.type.base
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcore.string.type.base',
  'type' => 'pbcore.string.type.base',
  'description' => 'A pbcore.string.type.base data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    
if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  ///////////////////////////////////
  // CONTENT TYPE:  pbcore.threeletterstring.type.base
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcore.threeletterstring.type.base',
  'type' => 'pbcore.threeletterstring.type.base',
  'description' => 'A pbcore.threeletterstring.type.base data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    
if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
////////////////////////////
// Nested complex types ...
//
    
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  PBCoreDescriptionDocument
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'PBCoreDescriptionDocument',
  'type' => 'pbcoredescriptiondocument',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '"The Master Container assembles together all collections
            of PBCore knowledge items. For PBCore these knowledge items are metadata descriptions of
            media. The MasterContainer is expressed as a document that hierarchically structures all
            the knowledge items and metadata terms and values related to a single data record
            associated with a media item. In our XML Schema Definition, the MasterContainer is
            referred to as the PBCoreDescriptionDocument."',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    
          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreIdentifier'.
// xsd:element 
$group = array (
  'label' => 'pbcoreIdentifier',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreidentifier',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreIdentifier'.
// xsd:element 

$field = array (
    'label' => 'pbcoreIdentifier',
    'field_name' => 'field_pbcoreidentifier',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreidentifier';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreTitle'.
// xsd:element 
$group = array (
  'label' => 'pbcoreTitle',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoretitle',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreTitle'.
// xsd:element 

$field = array (
    'label' => 'pbcoreTitle',
    'field_name' => 'field_pbcoretitle',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoretitle';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreSubject'.
// xsd:element 
$group = array (
  'label' => 'pbcoreSubject',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoresubject',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreSubject'.
// xsd:element 

$field = array (
    'label' => 'pbcoreSubject',
    'field_name' => 'field_pbcoresubject',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoresubject';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreDescription'.
// xsd:element 
$group = array (
  'label' => 'pbcoreDescription',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoredescription',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreDescription'.
// xsd:element 

$field = array (
    'label' => 'pbcoreDescription',
    'field_name' => 'field_pbcoredescription',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoredescription';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreGenre'.
// xsd:element 
$group = array (
  'label' => 'pbcoreGenre',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoregenre',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreGenre'.
// xsd:element 

$field = array (
    'label' => 'pbcoreGenre',
    'field_name' => 'field_pbcoregenre',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoregenre';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreRelation'.
// xsd:element 
$group = array (
  'label' => 'pbcoreRelation',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorerelation',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreRelation'.
// xsd:element 

$field = array (
    'label' => 'pbcoreRelation',
    'field_name' => 'field_pbcorerelation',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorerelation';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreCoverage'.
// xsd:element 
$group = array (
  'label' => 'pbcoreCoverage',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorecoverage',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreCoverage'.
// xsd:element 

$field = array (
    'label' => 'pbcoreCoverage',
    'field_name' => 'field_pbcorecoverage',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorecoverage';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreAudienceLevel'.
// xsd:element 
$group = array (
  'label' => 'pbcoreAudienceLevel',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreaudiencelevel',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreAudienceLevel'.
// xsd:element 

$field = array (
    'label' => 'pbcoreAudienceLevel',
    'field_name' => 'field_pbcoreaudiencelevel',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreaudiencelevel';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreAudienceRating'.
// xsd:element 
$group = array (
  'label' => 'pbcoreAudienceRating',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreaudiencerating',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreAudienceRating'.
// xsd:element 

$field = array (
    'label' => 'pbcoreAudienceRating',
    'field_name' => 'field_pbcoreaudiencerating',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreaudiencerating';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreCreator'.
// xsd:element 
$group = array (
  'label' => 'pbcoreCreator',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorecreator',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreCreator'.
// xsd:element 

$field = array (
    'label' => 'pbcoreCreator',
    'field_name' => 'field_pbcorecreator',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorecreator';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreContributor'.
// xsd:element 
$group = array (
  'label' => 'pbcoreContributor',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorecontributor',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreContributor'.
// xsd:element 

$field = array (
    'label' => 'pbcoreContributor',
    'field_name' => 'field_pbcorecontributor',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorecontributor';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcorePublisher'.
// xsd:element 
$group = array (
  'label' => 'pbcorePublisher',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorepublisher',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcorePublisher'.
// xsd:element 

$field = array (
    'label' => 'pbcorePublisher',
    'field_name' => 'field_pbcorepublisher',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorepublisher';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreRightsSummary'.
// xsd:element 
$group = array (
  'label' => 'pbcoreRightsSummary',
  'group_type' => 'standard',
  'group_name' => 'group_pbcorerightssummary',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreRightsSummary'.
// xsd:element 

$field = array (
    'label' => 'pbcoreRightsSummary',
    'field_name' => 'field_pbcorerightssummary',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcorerightssummary';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreInstantiation'.
// xsd:element 
$group = array (
  'label' => 'pbcoreInstantiation',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreinstantiation',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreInstantiation'.
// xsd:element 

$field = array (
    'label' => 'pbcoreInstantiation',
    'field_name' => 'field_pbcoreinstantiation',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreinstantiation';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreExtension'.
// xsd:element 
$group = array (
  'label' => 'pbcoreExtension',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreextension',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreExtension'.
// xsd:element 

$field = array (
    'label' => 'pbcoreExtension',
    'field_name' => 'field_pbcoreextension',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreextension';
  


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreIdentifier
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreIdentifier',
  'type' => 'pbcoreidentifier',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'identifier'.
// xsd:element 

$field = array (
    'label' => 'identifier',
    'field_name' => 'field_identifier',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor identifier is used to
                              reference or identify the entire record of metadata descriptions for a
                              media item and exists at the top level for a PBCore description and
                              its associated description document (XML). Best practice is to
                              identify the media item (whether analog or digital) by means of an
                              unambiguous string or number corresponding to an established or formal
                              identification system if one exists. Otherwise, use an identification
                              method that is in use within your agency, station, production company,
                              office, or institution.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'identifierSource'.
// xsd:element 

$field = array (
    'label' => 'identifierSource',
    'field_name' => 'field_identifiersource',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor identifierSource is
                              used in combination with the unambiguous reference or identifier for a
                              media item found in the descriptor identifier. Thus PBCore provides
                              not only a locator number, but also an agency or institution who
                              assigned it. Both exist at the top level for a PBCore description and
                              its associated description document (XML).\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreTitle
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreTitle',
  'type' => 'pbcoretitle',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'title'.
// xsd:element 

$field = array (
    'label' => 'title',
    'field_name' => 'field_title',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor title is a name given
                              to the media item you are cataloging. It is the unique name everyone
                              should use to refer to or search for a particular media item. There
                              are obviously many types of titles a media item may have, such as a
                              series title, episode title, segment title, or project title. Use the
                              descriptor titleType to indicate the type of title you are assigning
                              to the media item.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'titleType'.
// xsd:element 

$field = array (
    'label' => 'titleType',
    'field_name' => 'field_titletype',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor titleType is a
                              companion metadata field associated with the descriptor title. For a
                              title you give to a media item, you may wish to inform end users what
                              type of title it is (see the picklist of recommended vocabulary
                              terms).\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreSubject
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreSubject',
  'type' => 'pbcoresubject',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'subject'.
// xsd:element 

$field = array (
    'label' => 'subject',
    'field_name' => 'field_subject',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor subject is used to
                              assign topical headings or keywords that portray the intellectual
                              content of the media item you are cataloging. Typically, a subject is
                              expressed by a limited number of keywords, key phrases, or even
                              specific classification codes. Controlled vocabularies, authorities,
                              or formal classification schemes may be employed when assigning
                              descriptive subject terms (rather than using random or ad hoc
                              terminology).\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'subjectAuthorityUsed'.
// xsd:element 

$field = array (
    'label' => 'subjectAuthorityUsed',
    'field_name' => 'field_subjectauthorityused',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"If subjects are assigned to a media
                              item using the descriptor subject and the terms used are derived from
                              a specific authority or classification scheme, use
                              subjectAuthorityUsed to identify whose vocabularies and terms were
                              used.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreDescription
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreDescription',
  'type' => 'pbcoredescription',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'description'.
// xsd:element 

$field = array (
    'label' => 'description',
    'field_name' => 'field_description',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The metadata element description uses
                              free-form text or a narrative to report general notes, abstracts, or
                              summaries about the intellectual content of a media item you are
                              cataloguing. The information may be in the form of a paragraph giving
                              an individual program description, anecdotal interpretations, or brief
                              content reviews. The description may also consist of outlines, lists,
                              bullet points, rundowns, edit decision lists, indexes, or tables of
                              content.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'descriptionType'.
// xsd:element 

$field = array (
    'label' => 'descriptionType',
    'field_name' => 'field_descriptiontype',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor descriptionType is a
                              companion metadata field to the element description. The purpose of
                              descriptionType is to identify the nature of the actual description
                              and flag the form of presentation for the information.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreGenre
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreGenre',
  'type' => 'pbcoregenre',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'genre'.
// xsd:element 

$field = array (
    'label' => 'genre',
    'field_name' => 'field_genre',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor genre describes the
                              manner in which the intellectual content of a media item is presented,
                              viewed or heard by a user. It indicates the structure of the
                              presentation, as well as the topical nature of the content in a
                              generalized form.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'genreAuthorityUsed'.
// xsd:element 

$field = array (
    'label' => 'genreAuthorityUsed',
    'field_name' => 'field_genreauthorityused',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"If genre keywords are assigned to a
                              media item using the descriptor genre and the terms used are derived
                              from a specific authority or classification scheme, use
                              genreAuthorityUsed to identify whose vocabularies and terms were used.
                              PBcore supplies its own picklist of terms, but others may be employed
                              as long as the authority for a picklist is identified.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreRelation
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreRelation',
  'type' => 'pbcorerelation',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'relationType'.
// xsd:element 

$field = array (
    'label' => 'relationType',
    'field_name' => 'field_relationtype',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor relationType identifies
                              the type of intellectual content bond between a media item you are
                              cataloging and some other related media item.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'relationIdentifier'.
// xsd:element 

$field = array (
    'label' => 'relationIdentifier',
    'field_name' => 'field_relationidentifier',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"Once the type of relationship between
                              two media items is identified byusing the descriptor relationType,
                              then this companion descriptor relationIdentifier is used to provide a
                              name, locator, accession, identification number or ID where the
                              related item can be obtained or found. The cross reference uses a
                              unique identifier.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreCoverage
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreCoverage',
  'type' => 'pbcorecoverage',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'coverage'.
// xsd:element 

$field = array (
    'label' => 'coverage',
    'field_name' => 'field_coverage',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => ' \"The descriptor coverage uses keywords
                              to identify a span of space or time that is expressed by the
                              intellectual content of a media item. Coverage in intellectual content
                              may be expressed spatially by geographic location. Actual place names
                              may be used. Numeric coordinates and geo-spatial data are also
                              allowable, if useful or supplied. Coverage in intellectual content may
                              also be expressed temporally by a date, period, era, or time-based
                              event. The PBCore metadata element coverage houses the actual spatial
                              or temporal keywords. The companion descriptor coverageType is used to
                              identify the type of keywords that are being used.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'coverageType'.
// xsd:element 
$group = array (
  'label' => 'coverageType',
  'group_type' => 'standard',
  'group_name' => 'group_coveragetype',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'coverageType'.
// xsd:element 

$field = array (
    'label' => 'coverageType',
    'field_name' => 'field_coveragetype',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_coveragetype';
  


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  coverageType
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'coverageType',
  'type' => 'coveragetype',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    
if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreAudienceLevel
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreAudienceLevel',
  'type' => 'pbcoreaudiencelevel',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'audienceLevel'.
// xsd:element 

$field = array (
    'label' => 'audienceLevel',
    'field_name' => 'field_audiencelevel',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor audienceLevel
                              identifies a type of audience, viewer, or listener for whom the media
                              item you are cataloging is primarily designed or educationally
                              useful.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreAudienceRating
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreAudienceRating',
  'type' => 'pbcoreaudiencerating',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'audienceRating'.
// xsd:element 

$field = array (
    'label' => 'audienceRating',
    'field_name' => 'field_audiencerating',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor audienceRating
                              designates the type of users for whom a media item is intended or
                              judged appropriate in terms of its intellectual content. Standard
                              ratings have been crafted by the broadcast television and film
                              industries and are used as flags for audience or age-appropriate
                              materials.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreCreator
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreCreator',
  'type' => 'pbcorecreator',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'creator'.
// xsd:element 

$field = array (
    'label' => 'creator',
    'field_name' => 'field_creator',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor creator identifies a
                              person or organization primarily responsible for creating a media
                              item. The creator may be considered an author and could be one or more
                              people, a business, organization, group, project or service.\"
                           ',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'creatorRole'.
// xsd:element 

$field = array (
    'label' => 'creatorRole',
    'field_name' => 'field_creatorrole',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"Use the descriptor creatorRole to
                              identify the role played by the person or group identified in the
                              companion descriptor creator. Unlike print resources, there is usually
                              no single role, like an author, who has primary responsibility for the
                              creation of media items such as audio, video, film assets, and their
                              digital renditions. For these media, creators can fill many different
                              roles, such as the instructor for a video course, the interviewee from
                              a video history program, or the director of a program or film (if they
                              are identified as the primary creator for a media item).\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreContributor
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreContributor',
  'type' => 'pbcorecontributor',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'contributor'.
// xsd:element 

$field = array (
    'label' => 'contributor',
    'field_name' => 'field_contributor',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor contributor identifies
                              a person or organization that has made substantial creative
                              contributions to the intellectual content within a media item. This
                              contribution is considered to be secondary to the primary author(s)
                              (person or organization) identified in the descriptor
                           creator.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'contributorRole'.
// xsd:element 

$field = array (
    'label' => 'contributorRole',
    'field_name' => 'field_contributorrole',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"Use the descriptor contributorRole to
                              identify the role played by the person or group identified in the
                              companion descriptor contributor.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcorePublisher
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcorePublisher',
  'type' => 'pbcorepublisher',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'publisher'.
// xsd:element 

$field = array (
    'label' => 'publisher',
    'field_name' => 'field_publisher',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor publisher identifies a
                              person or organization primarily responsible for distributing or
                              making a media item available to others. The publisher may be a
                              person, a business, organization, group, project or
                           service.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'publisherRole'.
// xsd:element 

$field = array (
    'label' => 'publisherRole',
    'field_name' => 'field_publisherrole',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"Use the descriptor publisherRole to
                              identify the role played by the specific publisher or publishing
                              entity identified in the companion descriptor publisher.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreRightsSummary
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreRightsSummary',
  'type' => 'pbcorerightssummary',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'rightsSummary'.
// xsd:element 

$field = array (
    'label' => 'rightsSummary',
    'field_name' => 'field_rightssummary',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"Use the descriptor rightsSummary as an
                              all-purpose container field to identify information about copyrights
                              and property rights held in and over a media item, whether they are
                              open access or restricted in some way. If dates, times and
                              availability periods are associated with a right, include them. End
                              user permissions, constraints and obligations may also be identified,
                              as needed.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreInstantiation
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreInstantiation',
  'type' => 'pbcoreinstantiation',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'dateCreated'.
// xsd:element 

$field = array (
    'label' => 'dateCreated',
    'field_name' => 'field_datecreated',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"Use the descriptor dateCreated to
                              specify the creation date for a particular version or rendition of a
                              media item across its life cycle. It is the moment in time that the
                              media item was finalized during its production process and is
                              forwarded to other divisions or agencies to make it ready for
                              publication or distribution. A specific time may also be associated
                              with the date.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'dateIssued'.
// xsd:element 

$field = array (
    'label' => 'dateIssued',
    'field_name' => 'field_dateissued',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor dateIssued specifies
                              the formal date for a particular version or rendition of a media item
                              has been made ready or officially released for distribution,
                              publication or consumption. A specific time may also be associated
                              with the date.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatPhysical'.
// xsd:element 

$field = array (
    'label' => 'formatPhysical',
    'field_name' => 'field_formatphysical',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"Use the descriptor formatPhysical to
                              identify the format of a particular version or rendition of a media
                              item as it exists in an actual physical form that occupies physical
                              space (e.g., a tape on a shelf), rather than as a digital file
                              residing on a server or hard drive.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatDigital'.
// xsd:element 

$field = array (
    'label' => 'formatDigital',
    'field_name' => 'field_formatdigital',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"Use the descriptor formatDigital to
                              identify the format of a particular version or rendition of a media
                              item as it exists in its digital form, i.e., as a digital file on a
                              server or hard drive. Digital media formats may be expressed with
                              formal Internet MIME types.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatLocation'.
// xsd:element 

$field = array (
    'label' => 'formatLocation',
    'field_name' => 'field_formatlocation',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatLocation is
                              considered to be an \"address for a media item.\" For an organization or
                              producer acting as caretaker of a media resource, formatLocation may
                              contain information about a specific shelf location for an asset,
                              including an organization\'s name, departmental name, shelf ID and
                              contact information. The formatLocation for a data file or web page
                              may include domain, path, filename or html page.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatMediaType'.
// xsd:element 

$field = array (
    'label' => 'formatMediaType',
    'field_name' => 'field_formatmediatype',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatMediaType
                              identifies the general, high level nature of the content of a media
                              item. It uses categories that show how content is presented to an
                              observer, e.g., as a sound or text or moving image.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatGenerations'.
// xsd:element 

$field = array (
    'label' => 'formatGenerations',
    'field_name' => 'field_formatgenerations',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatGenerations
                              identifies the particular use or manner in which a version or
                              rendition of a media item is used, e.g., Audio/Narration or Moving
                              image/Backup master.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatStandard'.
// xsd:element 

$field = array (
    'label' => 'formatStandard',
    'field_name' => 'field_formatstandard',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"Use the descriptor formatStandard to
                              identify a larger technical system/standard or overarching media
                              architecture under which various media formats exist, e.g., NTSC is a
                              system/standard under which many video formats exist.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatEncoding'.
// xsd:element 

$field = array (
    'label' => 'formatEncoding',
    'field_name' => 'field_formatencoding',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatEncoding
                              identifies how the actual information in a media item is compressed,
                              interpreted, or formulated using a particular scheme. Identifying the
                              encoding used is beneficial for a number of reasons, including as a
                              way to achieve reversible compression; for the construction of
                              document indices to facilitate searching and access; or for efficient
                              distribution of the information across data networks with differing
                              bandwidths or pipeline capacities.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatFileSize'.
// xsd:element 

$field = array (
    'label' => 'formatFileSize',
    'field_name' => 'field_formatfilesize',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"Use the descriptor formatFileSize to
                              indicate the storage requirements or file size of a digital media
                              item. As a standard, express the file size in
                           bytes.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatTimeStart'.
// xsd:element 

$field = array (
    'label' => 'formatTimeStart',
    'field_name' => 'field_formattimestart',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatTimeStart
                              provides a time stamp for the beginning point of playback for a
                              time-based media item, such as digital video or audio. Use in
                              combination with formatDuration to identify a sequence or segment of a
                              media item that has a fixed start time and end
                           time.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatDuration'.
// xsd:element 

$field = array (
    'label' => 'formatDuration',
    'field_name' => 'field_formatduration',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatDuration provides
                              a timestamp for the overall length or duration of a time-based media
                              item. It represents the playback time.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatDataRate'.
// xsd:element 

$field = array (
    'label' => 'formatDataRate',
    'field_name' => 'field_formatdatarate',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatDataRate is
                              expresses the amount of data in a digital media file that is encoded,
                              delivered or distributed, for every second of time. Although optimal
                              data rates are often dependent on the codec used to compress and
                              encode a digital file, generally speaking, a larger data rate
                              translates into a better quality playback experience, for example 56
                              kilobits/second vs. 1 megabit/second.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatBitDepth'.
// xsd:element 

$field = array (
    'label' => 'formatBitDepth',
    'field_name' => 'field_formatbitdepth',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"For a media item (specifically, audio,
                              video, or image), the descriptor formatBitDepth measures \"How Much\"
                              data is sampled when information is digitized, encoded, or converted.
                              Bit depth is measured in bits and is an indicator of the perceived
                              viewing or playback quality of a media item (the higher the bit depth,
                              the greater the fidelity).\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatSamplingRate'.
// xsd:element 

$field = array (
    'label' => 'formatSamplingRate',
    'field_name' => 'field_formatsamplingrate',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"For a media item (specifically audio),
                              the descriptor formatSamplingRate measures \"How Often \" data is
                              sampled when information is digitized. For a digital audio signal, the
                              sampling rate is measured in kiloHertz and is an indicator of the
                              perceived playback quality of the media item (the higher the sampling
                              rate, the greater the fidelity).\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatFrameSize'.
// xsd:element 

$field = array (
    'label' => 'formatFrameSize',
    'field_name' => 'field_formatframesize',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatFrameSize
                              indicates the horizontal and vertical resolution of a format type. It
                              may be expressed in pixels, pixels per inch, or in the case of ATSC
                              digital TV, a combination of pixels measured horizontally vs. the
                              number of pixels of image/resolution data stacked vertically
                              (interlaced and progressive scan).\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatAspectRatio'.
// xsd:element 

$field = array (
    'label' => 'formatAspectRatio',
    'field_name' => 'field_formataspectratio',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatAspectRatio
                              indicates the ratio of horizontal to vertical proportions in the
                              display of an static image or moving image.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatFrameRate'.
// xsd:element 

$field = array (
    'label' => 'formatFrameRate',
    'field_name' => 'field_formatframerate',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatFrameRate
                              indicates the frames per second found in a video, motion sequence,
                              flash file, or animation\'s playback or display.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatColors'.
// xsd:element 

$field = array (
    'label' => 'formatColors',
    'field_name' => 'field_formatcolors',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatColors indicates
                              the overall color, grayscale, or black and white nature of a media
                              item, as a single occurrence or combination of occurrences in or
                              throughout the media item.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatTracks'.
// xsd:element 

$field = array (
    'label' => 'formatTracks',
    'field_name' => 'field_formattracks',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor formatTracks is simply
                              intended to indicate the number and type of tracks that are found in a
                              media item, whether it is analog or digital. For example, 1 video
                              track, 2 audio tracks, 1 text track, 1 sprite track, etc. Other
                              configuration information specific to these identified tracks should
                              be described using formatChannelConfiguration.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatChannelConfiguration'.
// xsd:element 

$field = array (
    'label' => 'formatChannelConfiguration',
    'field_name' => 'field_formatchannelconfiguration',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor
                              formatChannelConfiguration is designed to indicate the arrangement or
                              configuration of specific channels or layers of information within a
                              media item\'s tracks. Examples are 2-track mono, 8 track stereo, or
                              video track with alpha channel.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'language'.
// xsd:element 

$field = array (
    'label' => 'language',
    'field_name' => 'field_language',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor language identifies the
                              primary language of a media item\'s audio or text. Alternative audio or
                              text tracks and their associated languages should be identified using
                              the descriptor alternativeModes.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.threeletterstring.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'alternativeModes'.
// xsd:element 

$field = array (
    'label' => 'alternativeModes',
    'field_name' => 'field_alternativemodes',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor alternativeModes is a
                              catch-all metadata element that identifies equivalent alternatives to
                              the primary visual, sound or textual information that exists in a
                              media item. These are modes that offer alternative ways to see, hear,
                              and read the content of a media item. Examples include DVI
                              (Descriptive Video Information), SAP (Supplementary Audio Program),
                              ClosedCaptions, OpenCaptions, Subtitles, Language Dubs, and
                              Transcripts. For each instance of available alternativeModes, the mode
                              and its associated language should be identified together, if
                              applicable. Examples include \'SAP in English,\' \'SAP in Spanish,\'
                              \'Subtitle in French,\' \'OpenCaption in Arabic.\'\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreDateAvailable'.
// xsd:element 
$group = array (
  'label' => 'pbcoreDateAvailable',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoredateavailable',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreDateAvailable'.
// xsd:element 

$field = array (
    'label' => 'pbcoreDateAvailable',
    'field_name' => 'field_pbcoredateavailable',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoredateavailable';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreFormatID'.
// xsd:element 
$group = array (
  'label' => 'pbcoreFormatID',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreformatid',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreFormatID'.
// xsd:element 

$field = array (
    'label' => 'pbcoreFormatID',
    'field_name' => 'field_pbcoreformatid',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreformatid';
  


  $content['fields'][] = $field;

          // Complex types will be rendered as CCK Groups 
          

////////
// GROUP 'pbcoreAnnotation'.
// xsd:element 
$group = array (
  'label' => 'pbcoreAnnotation',
  'group_type' => 'standard',
  'group_name' => 'group_pbcoreannotation',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;


////////
// FIELD 'pbcoreAnnotation'.
// xsd:element 

$field = array (
    'label' => 'pbcoreAnnotation',
    'field_name' => 'field_pbcoreannotation',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  $field['group'] = 'group_pbcoreannotation';
  


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreDateAvailable
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreDateAvailable',
  'type' => 'pbcoredateavailable',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'dateAvailableStart'.
// xsd:element 

$field = array (
    'label' => 'dateAvailableStart',
    'field_name' => 'field_dateavailablestart',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor
                                       dateAvailableStart specifies a specific start date for the
                                       availability of a version or rendition of a media item. It
                                       may refer to start dates for the availability of a program
                                       that is broadcast locally, regionally, nationally or
                                       internationally, or for web-based distribution. A specific
                                       time may also be associated with the
                                    date.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'dateAvailableEnd'.
// xsd:element 

$field = array (
    'label' => 'dateAvailableEnd',
    'field_name' => 'field_dateavailableend',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor
                                       dateAvailableEnd specifies a specific end date for the
                                       availability of a version or rendition of a media item. It
                                       may refer to end dates for the availability of a program that
                                       is broadcast locally, regionally, nationally or
                                       internationally, or for web-based distribution. A specific
                                       time may also be associated with the
                                    date.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreFormatID
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreFormatID',
  'type' => 'pbcoreformatid',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'formatIdentifier'.
// xsd:element 

$field = array (
    'label' => 'formatIdentifier',
    'field_name' => 'field_formatidentifier',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor
                                       formatIdentifier employs an unambiguous reference or
                                       identifier for a particular rendition/instantiation of a
                                       media item. Best practice is to identify the media item
                                       (whether analog or digital) by means of a string or number
                                       corresponding to an established or formal identification
                                       system if one exists. Otherwise, use an identification method
                                       that is in use within your agency, station, production
                                       company, office, or institution.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'formatIdentifierSource'.
// xsd:element 

$field = array (
    'label' => 'formatIdentifierSource',
    'field_name' => 'field_formatidentifiersource',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor
                                       formatIdentifierSource is used in combination with the
                                       unambiguous reference or identifier for a
                                       rendition/instantiation of a media item as found in the
                                       descriptor formatIdentifier. Thus PBCore provides not only a
                                       locator number, but also indicates an agency or institution
                                       who assigned it.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreAnnotation
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreAnnotation',
  'type' => 'pbcoreannotation',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'annotation'.
// xsd:element 

$field = array (
    'label' => 'annotation',
    'field_name' => 'field_annotation',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor annotation is
                                       a stand-alone PBCore element where you can catalog any
                                       supplementary information about a media item or the metadata
                                       used to describe it. annotation clarifies element values,
                                       terms, descriptors, and vocabularies that may not be
                                       otherwise sufficiently understood.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  
  // xsd:element as complexType
    
  ///////////////////////////////////
  // CONTENT TYPE:  pbcoreExtension
  //  
unset($content);

    

$content['type']  = array (
  'name' => 'pbcoreExtension',
  'type' => 'pbcoreextension',
  'description' => 'A  data unit',
  'title_label' => 'Title',
  
  'min_word_count' => '0',
  'help' => '',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    

////////
// FIELD 'extension'.
// xsd:element 

$field = array (
    'label' => 'extension',
    'field_name' => 'field_extension',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"The descriptor extension provides
                              metadata descriptions crafted into metadata dictionaries and schemas
                              outside of the PBCore Metadata Dictionary Project. These extensions
                              fulfill the metadata requirements for communities identifying and
                              describing their own types of media with specialized, custom
                              terminologies.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this xsd:string field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;


////////
// FIELD 'extensionAuthorityUsed'.
// xsd:element 

$field = array (
    'label' => 'extensionAuthorityUsed',
    'field_name' => 'field_extensionauthorityused',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '\"If metadata extensions to PBCore are
                              assigned to a media item with the descriptor extension, and the terms
                              used are derived from a specific authority or metadata scheme, use
                              extensionAuthorityUsed to identify whose metadata extensions are being
                              used.\"',
    'required' => 0,
    'multiple' => '',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  
  // According to the schema, this pbcore.string.type.base field should probably be a more structured than plaintext.
      


  $content['fields'][] = $field;

if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////