<?php
/**
 * @file Utility function to import XSD Schemas into CCK Content types
 *
 * An XSD Schema defines the structure of an XML data file or packet.
 * It's likely that we want to represent that data structure in Drupal, in a way
 * as close to the original as possible, eg if an XSD defines a purchase order,
 * with address, item amd price fields, we can use a CCK node with address,
 * order and price fields.
 * This utility attempts to map from a formal XSD into a similar CCK sontent
 * type with named fields.
 *
 * Requires XSL support
 *
 * Somewhat convoluted to work around the 'batch' api - The CCK Creations were
 * timing out.
 *
 * There are some redundant (uncalled) functions still here. This module used to
 * be a stand-alone with its own UI. The UI aspect has now moved into
 * field_schema_importer, and this (the original inspiration) now just provides parsing
 * services to that.
 *
 * @author dman http://coders.co.nz/
 */


/**
 * Uses a given XSD Schema Document to programatically deduce and create CCK
 * Content types.
 *
 * Converts between several wildly different formats in the process:
 * XML/XSD -> processed through XSL to produce -> PHP string to be eval-ed ->
 * PHP Array of content type definitions -> Sent as an argument to a drupal form
 * -> creates real CCK def object.
 *
 * The PHP string used in the middle of the process is the same as that used by
 * the CCK content_import functions.
 *
 * @param $xsd_file full path to the xsd file. Can be an URL
 * @param $type_names array of named content type to be imported. If not set,
 * all valid objects will be imported
 *
 * @param $context - may be called from a batch job. If so, update a useful
 * message in the context
 */
function xsd2cck_import_cck_from_xsd($xsd_file, $type_names = array(), &$context = NULL) {
  $schemaversionnamespaceuri = xsd2cck_detect_namespace($xsd_file);
  $schemaversionxsls = array(
    'http://www.w3.org/2001/XMLSchema' => 'xsd-to-cck_6.xsl',
    'http://www.w3.org/2000/10/XMLSchema' => 'xsd2000-to-cck_6.xsl',
  );
  if (isset($schemaversionxsls[$schemaversionnamespaceuri])) {
    $xsl_path = dirname(__FILE__) . '/' . $schemaversionxsls[$schemaversionnamespaceuri];
  }
  else {
    drupal_set_message("Problem analyzing the xsd file. Schema version '$schemaversionnamespaceuri' is unknown. Different versions of the spec may require different rules, only (" . join(array_keys($schemaversionxsls), ',') . ") are currently supported");
    return FALSE;
  }
  drupal_set_message("XML Schema version is <pre>$schemaversionnamespaceuri</pre>, therefore using " . basename($xsl_path));
  if (! is_readable($xsl_path)) {
    drupal_set_message("Problem accessing transformation file '$xsl_path", 'error');
    return FALSE;
  }

  $cck_def_string = xsd2cck_xsl_transform($xsl_path, $xsd_file);

  // drupal_set_message('PHP transformation is <pre>' .$cck_def_string .'</pre>');
  // The cck_def_string is PHP code that creates and defines a big $content_types array for us.

  global $content_types;
  @eval($cck_def_string); // running eval here should import the results into current context
  if (empty($content_types)) {
    drupal_set_message(t("Problem parsing any CCK definitions from the given CCK Import. No top-level content types detected. Possibly a syntax error in the transformed code. Transformed schema file" . $xsd_file), 'error');
    return;
  }

  drupal_set_message('Available Type definitions: <pre>' . print_r(array_keys($content_types), 1) . '</pre>');
  if (empty($type_names)) {
    // Process all items found in the Schema.
    $type_names = array_keys($content_types);
  }

  foreach ($type_names as $type_name) {
    if (! empty($content_types[$type_name]) ) {
      xsd2cck_create_content_from_def($content_types[$type_name]);
      $context['message'] = "Creating Content Type: " . $type_name;
    }
    else {
      drupal_set_message(t("Failed to find a definition of the requested Content type %type_name in the imported schema file %xsd_file", array('%type_name' => $type_name, '%xsd_file' => $xsd_file)), 'error');
    }
  }
}

/**
 * Returns an array defining one or more bundles.
 * @param string $xsd_source
 */
function convert_xsd_string_to_cck_content_type($xsd_source) {
  $schemaversionnamespaceuri = xsd2cck_detect_namespace($xsd_source);
  $schemaversionxsls = array(
    'http://www.w3.org/2001/XMLSchema' => 'xsd-to-cck_6.xsl',
    'http://www.w3.org/2000/10/XMLSchema' => 'xsd2000-to-cck_6.xsl',
  );
  if (isset($schemaversionxsls[$schemaversionnamespaceuri])) {
    $xsl_path = dirname(__FILE__) . '/' . $schemaversionxsls[$schemaversionnamespaceuri];
  }
  else {
    drupal_set_message("Problem analyzing the xsd file. Schema version '$schemaversionnamespaceuri' is unknown. Different versions of the spec may require different rules, only (" . join(array_keys($schemaversionxsls), ',') . ") are currently supported", 'warning');
    return FALSE;
  }
  drupal_set_message("XML Schema version is <pre>$schemaversionnamespaceuri</pre>, therefore using " . basename($xsl_path));
  if (! is_readable($xsl_path)) {
    drupal_set_message("Problem accessing transformation file '$xsl_path", 'error');
    return FALSE;
  }
  $cck_def_string = xsd2cck_xsl_transform($xsl_path, $xsd_source);

  drupal_set_message('PHP transformation is <pre>' . htmlentities($cck_def_string) . '</pre>');
  // The cck_def_string is PHP code that creates and defines a big $content_types array for us.

  global $content_types;
  @eval($cck_def_string); // running eval here should import the results into current context
  if (empty($content_types)) {
    drupal_set_message(t("Problem parsing any CCK definitions from the given CCK Import. No top-level content types detected. Possibly a syntax error in the transformed code. Transformed schema file <pre>$xsd_source</pre>"), 'error');
    return;
  }
  return $content_types;
}



/**
 * Programatically create content type and fields from a 'content_copy' style
 * array.
 *
 * This version is batch-safe! Needed as creating a CCK seems to take a lot of
 * DB time, and times out when doing more than a couple of fields :(
 */
function xsd2cck_create_content_from_def($cck_definition_array, $use_batch = TRUE) {
  $type_name = $cck_definition_array['type']['type']; // note the type is machinename, not human name
  watchdog('XSD-to-CCK', "Creating a content type " . $cck_definition_array['type']['name']);

  // There's no API for creating node types, need to submit a form.
  // Drupal_execute does not behave well under batch processes.
  // It defers the action OK, but doesn't save the made-up form state.
  // Need to call submit directly instead, skipping to the good bits of drupal_execute
  // @see content_copy_import_form_submit

  // Create the content type
  module_load_include('inc', 'node', 'content_types');
  $form_state = array('values' => $cck_definition_array['type']);
  $form = drupal_retrieve_form('node_type_form', $form_state);
  $form['#post'] = $form_state['values'];
  node_type_form_submit($form, $form_state);
  form_set_error(NULL, NULL, TRUE); // requires 6.5 to do the reset. otherwise just hope nothing goes wrong.

  if (isset($cck_definition_array['groups']) && module_exists('fieldgroup')) {
    $imported_groups = (array) $cck_definition_array['groups'];
    foreach ($imported_groups as $group) {
      $group_name = $group['group_name'];
      fieldgroup_save_group($type_name, $group);
    }
    // Reset the static variable in fieldgroup_groups() with new data.
    fieldgroup_groups('', FALSE, TRUE);
  }


  // Add each field, batched or inline
  $fields = $cck_definition_array['fields'];
  foreach ($fields as $i => $field) {
    $fields[$i]['type_name'] = $type_name;
  }

  if ($use_batch) {
    xsd2cck_add_field_defs_to_queue($fields, $type_name);
  }
  else {
    // Just do it now. (not good for more than a dozen fields at once)
    foreach ($fields as $field) {
      $field = content_field_instance_create($field);
    }
  }
  content_clear_type_cache();
}

/**
 * Use batch operations to create a set of fields
 *
 * @param $fields - a list of field defs, as provided by cck content export.
 */
function xsd2cck_add_field_defs_to_queue($fields, $type_name) {
  // Prepare a batch config
  $batch_settings = array(
    'title' => t('Setting up CCK fields on ' . $type_name),
    'init_message' => t('Starting batch field creation for %type_name.', array('%type_name' => $type_name)),
    'progress_message' => t('Added field # @current out of @total on %type_name.', array('%type_name' => $type_name)),
    #'file' => drupal_get_path('module', 'content') . '/includes/content.crud.inc' ,
    'file' => __FILE__,
  );

  // Make sure the field doesn't already exist in the type. content_field_instance_create() fails to check properly
  $content_info = _content_type_info();

  foreach ($fields as $field_i => $field) {
    $field_name = $field['field_name'];
    $content_type_name = $field['type_name'];
    if (isset($content_info['content types'][$content_type_name]['fields'][$field_name])) {
      // Skip
      drupal_set_message(t("%content_type_name -> %field_name already exists on this type. Skipping it", array("%content_type_name" => $content_type_name, "%field_name" => $field_name)));
    }
    else {
      #$batch_operations[$field_name] = array('content_field_instance_create', array($field));
      $batch_operations[$field_name] = array('xsd2cck_field_instance_create', array($field));
    }
  }

  // Queue up our ops,.
  $batch_settings['operations'] = $batch_operations;

  // Trigger the ops
  batch_set($batch_settings);
}

/**
 * Create the given field definition.
 *
 * Designed to be run as a batch API callback.
 *
 * Wrapper to content_field_instance_create() to ensure the required bits are
 * loaded, and provide more feedback.
 * @see content_field_instance_create()
 */
function xsd2cck_field_instance_create($field, &$context) {
  module_load_include('inc', 'content', 'includes/content.crud');
  $context['message'] = "Creating Field: " . $field['type_name'] . ' -> ' . $field['label'];
  content_field_instance_create($field);
}

/**
 * Run the XSL Transformation on the two given filenames
 *
 * @ingroup utility
 */
function xsd2cck_xsl_transform($xsl_path, $xml_path) {
  if (extension_loaded("xsl")) {
    $parameters = array(); // placeholder for later tweaks

    $xmldoc = new domdocument;
    if (file_exists($xml_path)) {
      if (! $xmldoc->load($xml_path) ) {
        trigger_error("Failed to parse in xml source. [$xml_path]", E_USER_WARNING);
        return;
      }
    }
    else {
      // Load from full string
      if (! $xmldoc->loadXML($xml_path) ) {
        trigger_error("Failed to parse in xml source. [Full XML string]", E_USER_WARNING);
        return;
      }
    }

    $xsldoc = new domdocument;
    if (! $xsldoc->load($xsl_path) ) {
      trigger_error("Failed to parse in xml source. [$xsl_path]", E_USER_WARNING);
      return;
    }
    // Some files need to know where they came from (XSL includes)
    $xsldoc->documenturi = $xsl_path;

    $xsltproc = new XSLTProcessor;
    $xsltproc->importStyleSheet($xsldoc); // attach the xsl rules

    // Set parameters when defined
    if ($parameters) {
      foreach ($parameters as $param => $value) {
        $xsltproc->setParameter("", $param, $value);
      }
    }

    return $xsltproc->transformtoxml($xmldoc);
  }
  trigger_error("No XSL support found.", E_USER_WARNING);
}

/**
 * Heck. We may need to support different versions of XMLSchema,
 *
 * 'www.w3.org/2001/XMLSchema',
 * 'www.w3.org/2000/10/XMLSchema',
 *
 * The XSL can't handle both at onces (without becoming horrible) so we need to
 * switch between two transformation docs, after inspecting the source doc.
 *
 */
function xsd2cck_detect_namespace($xml_path) {
  $xmldoc = new domdocument;

  // Load from either full string or filepath
  if (file_exists($xml_path)) {
    if (! $xmldoc->load($xml_path) ) {
      trigger_error("Failed to parse in xml source. [$xml_path]", E_USER_WARNING);
      return;
    }
  }
  else {
    if (! $xmldoc->loadxml($xml_path) ) {
      trigger_error("Failed to parse in xml source. [Full XML input]", E_USER_WARNING);
      return;
    }
  }
  // Find the namespace URI of the top-level "??:schema" element.
  $schemanodes = $xmldoc->getElementsByTagName('schema');
  foreach ($schemanodes as $schemanode) {
    // probably just one, but whatever
    return $schemanode->namespaceURI;
  }
  drupal_set_message("Document did not appear to contain any 'schema' node element, I can't therefore find the namespace.", 'warning');
}

