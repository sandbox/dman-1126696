<?xml version="1.0"?>
 <xsl:stylesheet version="1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.w3.org/2007/schema-for-xslt20.xsd
    http://www.w3.org/2007/schema-for-xslt20.xsd"

  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:xsd="http://www.w3.org/2000/10/XMLSchema"
  xmlns:dc="http://purl.org/dc/elements/1.1/"   

  >
  <!-- 
  THIS doc uses the older Schema specification:
  xsd="http://www.w3.org/2000/10/XMLSchema"
  Apart from that, it's pretty much the same as the preferred xsd-to-cck.
  
   -->
  <dc:title>Conversion from XML Schema definitions into Drupal CCK Content type definitions</dc:title>
  <dc:creator>Dan Morrison</dc:creator>
  <dc:publisher>coders.co.nz</dc:publisher>
  <dc:description>
  Many XML schemas are used to describe modular units of structured data.
  Drupal CCK Content types are modular units of structured data storage,
  with configurable 'fields' of various datatypes.
  This transformation document is designed to take an XML Schema Definition and 
  convert it into a corresponding Drupal Content Type definition.

  In theory, there can then be a 1:1 mapping of XML source data to Drupal Node serialized data.
  * The output is a PHP structured array, suitable to be imported using CCK6 'content import'
  * As yet, only a flat array of named text fields is supported.
  * It does not support the full range of XSD elements or parameters
  * complexContent is not supported
  </dc:description>
  <!--
  From an XSD,

  All named complexType elements at the top level define a CCK Content type
  All element definitions which contain anonymous complex types define a CCK content type.
  
  All xsd:elements within a sequence inside a complexType define respective fields.
  Fields that are themselves references to complexTypes or other types become nodereferences.
  xsd:elements that name a @type are nodereferences to content of that type
  All other fields become textfields
  
  -->

	<xsl:output method="text" encoding="UTF-8" />
  <xsl:variable name="uppercase">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
  <xsl:variable name="lowercase">abcdefghijklmnopqrstuvwxyz</xsl:variable>


  <xsl:template name="schema" match="xsd:schema">
$content_types = array();
  <!--  Process all top level elements -->
    <xsl:apply-templates select="xsd:complexType|xsd:element" />

  <!--  Scan the doc for embedded interesting data structures -->
////////////////////////////
// Nested complex types ...
//
    <xsl:apply-templates select="//xsd:element[xsd:complexType]" />

  </xsl:template>


  
  <xsl:template name="dataSet" match="xsd:DataSet">
    <xsl:apply-templates select="*" />
  </xsl:template>


  <xsl:template name="ContentType" match="xsd:complexType">
    <!-- 
      XSD types have no descriptions :-/
    -->
    <xsl:param name="description" ><xsl:value-of select="xsd:annotation/xsd:documentation" /></xsl:param>
    <xsl:param name="name" ><xsl:value-of select="@name" /></xsl:param>
  ///////////////////////////////////
  // CONTENT TYPE:  <xsl:value-of select="$name" />
  //  
unset($content);

    <xsl:variable name="humanname" ><xsl:value-of select="$name" /></xsl:variable>
    <xsl:variable name="machinename" select="translate(translate($humanname, ' -:', '___'), $uppercase, $lowercase)" ></xsl:variable>

$content['type']  = array (
  'name' => '<xsl:value-of select="$name" />',
  'type' => '<xsl:value-of select="$machinename" />',
  'description' => 'A <xsl:value-of select="@name" /> data unit',
  'title_label' => 'Title',
  <!--
  // Removing the body label removes the default 'body' textarea.
  'body_label' => 'Body',
  -->
  'min_word_count' => '0',
  'help' => '<xsl:value-of select="$description" />',
  'node_options' => array ('status' => true, 'promote' => true, 'sticky' => false, 'revision' => false, ),
  'language_content_type' => 0,
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
);

$content['fields']  = array ();

    <xsl:for-each select="xsd:attribute|xsd:sequence/xsd:element|xsd:complexContent/xsd:extension/xsd:sequence/xsd:element" >
      <!--  note, I'm not sure what 'complexContent' is supposed to mean. I'll flatten it  -->
      <xsl:call-template name="fieldDef" />
    </xsl:for-each>

<!-- Don't bother with non-complex elements -->
if ($content['fields']) {
  $content_types[$content['type']['name']] = $content;
}
  //
  ///////////////////////////////////
  </xsl:template>


  <!--  
    elements which contain further embedded complex types are significant enough to be their own CCK content def.
    Invoke the complextype builder
   -->
  <xsl:template name="fullElement" match="xsd:element[xsd:complexType]" >
  // <xsl:value-of select="name(.)" /> as complexType
    <xsl:variable name="description"><xsl:value-of select="xsd:annotation/xsd:documentation" /></xsl:variable>
    <xsl:variable name="name"><xsl:value-of select="@name" /></xsl:variable>
    <xsl:for-each select="xsd:complexType">
	    <xsl:call-template name="ContentType" >
        <xsl:with-param name="description"><xsl:value-of select="$description" /></xsl:with-param>
        <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
	    </xsl:call-template>
    </xsl:for-each>
  </xsl:template>



  <!--  
    Elements which do NOT contain further embedded complex types are fields within the current context.
    Create a field definition.
   -->
  <xsl:template name="fieldDef" match="xsd:element|xsd:attribute" >
    <xsl:variable name="humanname" >
      <xsl:choose>
        <xsl:when test="@name"><xsl:value-of select="@name" /></xsl:when>
        <xsl:when test="@ref"><xsl:value-of select="@ref" /></xsl:when>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="machinename" select="translate(translate($humanname, ' -:', '___'), $uppercase, $lowercase)" ></xsl:variable>

    <!-- 
    Most fields will be rendered as text, to begin with.
    If the element contains further 'complextype' structure, 
    it is better modeled as a group, or possibly a REF to other node data structures of the named type.
    Do this as noderefs, not text.
    -->

    <!--
    Some known values, dc:title, dc:created, dc:modified
    are built in to the system. Skip them
    -->
    <xsl:choose>
      <xsl:when test="@ref = 'dcterms:created'"></xsl:when>
      <xsl:when test="@ref = 'dcterms:modified'"></xsl:when>
      <xsl:when test="@ref = 'dc:title'"></xsl:when>
      <xsl:otherwise>

      <!--  sanitize the description text. Quotes and things may break the PHP. -->
			<xsl:variable name="description" >
			<xsl:call-template name="safe_characters"><xsl:with-param name="text"><xsl:value-of select="xsd:annotation/xsd:documentation" /></xsl:with-param></xsl:call-template>
			</xsl:variable>  

      <!-- 
        Possibly this field is a container for a reference to other content (complex sequence)
        or has a defined 'type' which is a simple reference. Check if either is true.
       -->
      <xsl:variable name="targetType">
        <xsl:choose>
          <xsl:when test="xsd:complexType/xsd:complexContent/xsd:extension/@base">
            <xsl:value-of select="xsd:complexType/xsd:complexContent/xsd:extension/@base" />
          </xsl:when>
          <xsl:when test="xsd:complexType/xsd:sequence/xsd:element/@type">
            <xsl:value-of select="xsd:complexType/xsd:sequence/xsd:element/@type" />
          </xsl:when>
          <xsl:when test="xsd:complexType">
            <xsl:value-of select="@name" />
          </xsl:when>
          <xsl:when test="@type">
            <xsl:value-of select="@type" />
          </xsl:when>
        </xsl:choose>
      </xsl:variable>
      
      <xsl:variable name="multiple">
        <xsl:choose>
          <xsl:when test="xsd:complexType/xsd:sequence">1</xsl:when>
          <xsl:when test="xsd:sequence/xsd:element[@maxOccurs = 'unbounded']">1</xsl:when>
        </xsl:choose>
      </xsl:variable>

      <xsl:choose>

        <xsl:when test="xsd:complexType">
          // Complex types will be rendered as CCK Groups 
		      <xsl:call-template name="cckGroup" >  
		        <xsl:with-param name="type" select="$targetType"/>
		        <xsl:with-param name="humanname" select="$humanname" />
		        <xsl:with-param name="machinename" select="$machinename" />
		        <xsl:with-param name="description" select="$description" />
		      </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
		      <xsl:call-template name="cckField" >  
		        <xsl:with-param name="type" select="$targetType"/>
		        <xsl:with-param name="humanname" select="$humanname" />
		        <xsl:with-param name="machinename" select="$machinename" />
		        <xsl:with-param name="description" select="$description" />
		        <xsl:with-param name="multiple" select="$multiple" />
		      </xsl:call-template>
        </xsl:otherwise>

      </xsl:choose>


      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
 <!--  Elements which contain named complex types are really nodereferences to content of the named type. -->
  

<xsl:template name="cckField" >
	<xsl:param name="type" />
	<xsl:param name="humanname" />
	<xsl:param name="machinename" />
	<xsl:param name="description" />
	<xsl:param name="multiple" />
  <xsl:param name="groupname" />

////////
// FIELD '<xsl:value-of select="$humanname" />'.
// <xsl:value-of select="name(.)" /> 

$field = array (
    'label' => '<xsl:value-of select="$humanname" />',
    'field_name' => 'field_<xsl:value-of select="$machinename" />',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'rows' => 1,
    'description' => '<xsl:value-of select="$description" />',
    'required' => 0,
    'multiple' => '<xsl:value-of select="$multiple" />',
    'max_length' => '',
    'weight' => count($content['fields']),
    'module' => 'text',
    'widget_module' => 'text',
    'default_value' => array(),
    'columns' =>  array ( 'value' => array ('type' => 'text', 'size' => 'big', 'not null' => false, 'sortable' => true, ), ),
  );
  <xsl:if test="string($groupname)" >
  $field['group'] = '<xsl:value-of select="$groupname" />';
  </xsl:if>

    <xsl:if test="string($type)" >
  // According to the schema, this <xsl:value-of select="$type" /> field should probably be a more structured than plaintext.
      <xsl:choose>
        <xsl:when test="$type = 'xs:string'"></xsl:when>
        <xsl:when test="$type = 'xsd:string'"></xsl:when>
        <xsl:when test="$type = 'xs:token'"></xsl:when>
        <xsl:when test="$type = 'xsd:date'"></xsl:when>
        
        <xsl:when test="$type = 'xsd:decimal'">
  $field['type'] = 'number_decimal';
  $field['widget_type'] = 'number';
        </xsl:when>
        <xsl:when test="$type = 'xs:boolean' or $type = 'xsd:boolean' ">
  $field['type'] = 'number_integer';
  $field['widget_type'] = 'optionwidgets_onoff';
  $field['allowed_values'] = '0
1|<xsl:value-of select="$humanname" />';
  $field['widget_module'] = 'optionwidgets';
        </xsl:when>
        <xsl:otherwise>
<!-- 
This is more correct, but too complex for our needs right now.
Disable the referenced type lookups and just use text, as that's all we can really serialize

          <xsl:variable name="referenceable_types" select="translate(translate($type, ' -:', '___'), $uppercase, $lowercase)" ></xsl:variable>
  $field['type'] = 'nodereference';
  $field['widget_type'] = 'nodereference_select';
  $field['module'] = 'nodereference';
  $field['widget_module'] = 'nodereference';
  $field['referenceable_types'] = array (
      '<xsl:value-of select="$referenceable_types" />' => '<xsl:value-of select="$referenceable_types" />',
  );
-->
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>

    <xsl:if test="xsd:simpleType" >
    <!-- Enumerated type. Text, but with some restrictions -->
  $field['type'] = 'text';
  $field['module'] = 'text';
  $field['widget_type'] = 'optionwidgets_select';
  $field['widget_module'] = 'optionwidgets';
      <xsl:if test="xsd:simpleType/xsd:restriction/xsd:enumeration" >
  $field['allowed_values'] = "<xsl:for-each select="xsd:simpleType/xsd:restriction/xsd:enumeration"><xsl:value-of select="@value" /><xsl:text>
</xsl:text></xsl:for-each>";
      </xsl:if>
    </xsl:if>


  $content['fields'][] = $field;
</xsl:template>



<xsl:template name="cckGroup" >
  <xsl:param name="type" />
  <xsl:param name="humanname" />
  <xsl:param name="machinename" />
  <xsl:param name="description" />

////////
// GROUP '<xsl:value-of select="$humanname" />'.
// <xsl:value-of select="name(.)" /> 
$group = array (
  'label' => '<xsl:value-of select="$humanname" />',
  'group_type' => 'standard',
  'group_name' => 'group_<xsl:value-of select="$machinename" />',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset',
      'description' => '',
    ),
    'display' => array (
     'description' => '',
    ),
    'fields' => array (
    ),
  ),
);
$content['groups'][] = $group;
<!--  
Normally Complex Elements have a plaintext version of themselves.
Expose that as a field.
 -->
  <xsl:call-template name="cckField" >  
    <xsl:with-param name="humanname" select="$humanname" />
    <xsl:with-param name="machinename" select="$machinename" />
    <xsl:with-param name="description" select="$description" />
    <xsl:with-param name="groupname" >group_<xsl:value-of select="$machinename" /></xsl:with-param>
  </xsl:call-template>
<!--  

-->
</xsl:template>

  
  <!-- 
  UTILITY
   -->
<!--
 This template is used to escape the ' and " in text nodes that need
 to be assigned to javascript string variables. It replaces them with
 the javascript-standard \' and \"
-->
<xsl:template name="safe_characters">
	<!-- required -->
	<xsl:param name="text"/>
	
	<xsl:variable name="tmp">
	
		<xsl:call-template name="replace-substring">
		<xsl:with-param name="from" select="'&quot;'"/>
		<xsl:with-param name="to">\"</xsl:with-param>
		<xsl:with-param name="value">
		<xsl:call-template name="replace-substring">
		<xsl:with-param name="from">&apos;</xsl:with-param>
		<xsl:with-param name="to">\'</xsl:with-param>
		<xsl:with-param name="value" select="$text" />
		</xsl:call-template>
		</xsl:with-param>
		</xsl:call-template>
	
	</xsl:variable>
	<xsl:value-of select="$tmp" />
</xsl:template>

<xsl:template name="replace-substring">
      <xsl:param name="value" />
      <xsl:param name="from" />
      <xsl:param name="to" />
      <xsl:choose>
         <xsl:when test="contains($value,$from)">
            <xsl:value-of select="substring-before($value,$from)" />
            <xsl:value-of select="$to" />
            <xsl:call-template name="replace-substring">
               <xsl:with-param name="value" select="substring-after($value,$from)" />
               <xsl:with-param name="from" select="$from" />
               <xsl:with-param name="to" select="$to" />
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$value" />
         </xsl:otherwise>
      </xsl:choose>
</xsl:template>

</xsl:stylesheet>
