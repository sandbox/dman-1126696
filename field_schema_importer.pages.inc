<?php
/**
 * @file
 * UI and rendering for the schema importer
 *
 * Theme functions and FAPI processes
 *
 * @author 'dman' Dan Morrison http://coders.co.nz/
 * @version 2009-10
 */


/**
 * The main schema import form page.
 *
 * This contains various sub-form elements that are delegated to further form
 * functions.
 *
 * All it is constructed by parameters - the options to show, the validation
 * funcs to call, and what to do with it all once a source is parsed.
 */
function field_schema_importer_import_form($form, &$form_state) {
  // Use the importer_ui library.
  module_load_include('inc', 'field_schema_importer', '/importer_ui/importer_ui');

  // This config tells the importer_ui form how to build itself.
  $importer_ui_config = array(
    // A list of supported input sources.
    'services_callback' => 'field_schema_importer_services',
    // A list of supported parseable formats.
    'formats_callback'  => 'field_schema_importer_formats',
    // preview_callback takes parsed data derived from the sources and displays
    // it for editing and confirmation.
    'preview_callback'  => 'field_schema_importer_content_type_form',
    'process_callback'  => 'field_schema_importer_create_bundles',
  );
  $form = importer_ui_import_form($form_state, $importer_ui_config);
  return $form;
}


/**
 * All-in-one content type pre-import FAPI form.
 *
 * A subform (but quite a hefty one) that renders all the info needed to make a
 * content type in one place.
 * Used as an insertion into the field_schema_importer_form
 *
 * @param array $form_state
 *   FAPI.
 * @param array $data
 *   a content type $content arry, containing most of what we
 *   know about the content type being imported
 *
 * @return array
 *   FAPI form.
 */
function field_schema_importer_content_type_form(&$form_state, $data) {
  // $content_type is an export as produced by bundle_copy.
  // It is legal to have more than one bundle in the same file
  // but we will ignore that.
  // Later, this can be extended to support multiples by putting a
  // wrapper around this form.
  $bundle = reset($data['bundles']);

  if (empty($bundle)) {
    drupal_set_message("No bundle data to import? That's no good, start again.");
    // dpm(get_defined_vars(), __FUNCTION__);
    return FALSE;
  }

  // Many assumptions about it being a node not an entity may still be in here.

  // Perform fix-ups on the data structure if needed.
  module_load_include('inc', 'field_schema_importer', 'field_schema_importer.process');
  // These validates operate on the data directly.
  field_schema_importer_validate_content_type($bundle);
  field_schema_importer_validate_fields($data['fields'], $data['instances']);

  // This $form is never supposed to be invoked on its own, only as part of
  // parent callbacks or AHAH.
  $form = array(
    '#type'     => 'fieldset',
    '#tree'     => TRUE,
    // The fields heirarchy below here will be built
    // to reflect the field structure directly.
    '#title'    => "Content Type",
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'field_schema_importer') . '/field_schema_editor.css'),
      'js'  => array(
        drupal_get_path('module', 'field_schema_importer') . '/field_schema_editor.js',
        'misc/tableselect.js',
      ),
    ),
  );

  // Based on content_types.inc:node_type_form()
  $bundleform = array(
    '#type'        => 'fieldset',
    '#title'       => t('Base properties'),
    '#description' => t('
      This sub-form reflects the content type management as seen at
      /admin/content/node-type. You will be able to edit it there later.
    '),
  );

  $bundleform['name'] = array(
    '#title'         => 'Content type name',
    '#type'          => 'textfield',
    '#default_value' => $bundle->name,
  );
  $bundleform['type'] = array(
    '#title'         => 'Machine name',
    '#type'          => 'textfield',
    '#default_value' => $bundle->type,
  );
  $existing_content_type = node_type_get_type($bundle->type);
  if (!empty($existing_content_type)) {
    $bundleform['orig_type'] = array(
      '#title'         => 'Update existing type',
      '#type'          => 'checkbox',
      '#return_value'  => $bundle->type,
      '#default_value' => TRUE,
      '#description'   => t('A type of this name already exists.'),
    );
    drupal_set_message(t("
      A content type %type already exists - this will be updated
      (you can add extra fields to it using this UI)
      unless you choose not to.
    ", array('%type' => $bundle->type)), 'notice');
  }

  if (!empty($bundle->uri)) {
    $bundleform['type']['#suffix'] = l($bundle->uri, $bundle->uri);
  }

  if (!empty($bundle->description)) {
    $bundleform['description'] = array(
      '#title'         => 'Description',
      '#type'          => 'textarea',
      '#default_value' => $bundle->description,
    );
  }

  // If there was any other data in the content typedef we were given
  // it will be available in form storage.
  $form['bundles'][$bundle->type] = $bundleform;

  // Fields - filtered by field_instances, as that's what counts.
  // $fields = & $data['fields'];
  $instances = array();
  if (!empty($data['instances'])) {
    $instances = &$data['instances'];
  }
  // Now the fields. These 'fieldsets' I build here will actually be rendered
  // in a table form later.
  $form['fields'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Fields'),
    '#description' => t("
      This reflects the fields management provides by fields API.
      You will be able to edit them and change the order there later.
      However it's highly recommended you review each field here first,
      as it will save you time in repairs.
    "),
    '#theme'       => 'field_schema_importer_fieldsets_as_table',
    // For js magic:
    '#attributes'  => array('class' => array('typedef-fields')),
  );

  // Prepare the list of available known groups.
  $groups_list = array_map(create_function('$group', 'return $group->label;'), (array) @$data['fieldgroups']);
  $groups_list = array('' => '<none>') + $groups_list;

  // Tag the fields with their groups for the UI.
  foreach ((array) @$data['fieldgroups'] as $group) {
    // Loop through the groups,
    foreach ($instances as $field_name => &$field_instance_collection) {
      // Find field instances named in the list of children.
      if (!in_array($field_name, $group->children)) {
        continue;
      }
      // Tag the instance as a group member.
      foreach ($field_instance_collection as &$field_instance) {
        $field_instance['group'] = $group->identifier;
      }
    }
  }

  // See a raw data export from bundle_copy.module for the data structure
  // we have to iterate over here.
  foreach ($instances as $field_name => $field_instance_collection) {
    // To keep terminology the same as bundle_copy and core,
    // $field is the global field data type definition, while
    // $instance is details about haw it exists on this bundle.

    // Filter for the current bundle, just in case we get multiples one day.
    $field = NULL;
    foreach ($field_instance_collection as $field_instance) {
      if ($field_instance['bundle'] == $bundle->type) {
        $instance = $field_instance;
      }
    }
    if (empty($instance)) {
      // Found an unused field instance, ignore.
      continue;
    }

    // Get started now.
    // We need to know both field and instance.

    $field = $data['fields'][$field_name];
    // It's possible that the incoming bundle def didn't bother to define
    // the field fully? See if the system recognises the field name.
    if (!$field) {
      $field = field_read_field($field_name, array('include_inactive' => TRUE));
    }
    if (!$field) {
      drupal_set_message(t('Insufficient detail to create field instance %field_name', array('%field_name' => $field_name)), 'error');
      continue;
    }

    // If updating a content type, field creation becomes opt-in, not auto.
    if (!empty($existing_content_type)) {
      $instance['create'] = FALSE;
    }

    $field_subform = field_schema_importer_field_edit_form($field, $instance, $groups_list);
    // The field renderer will provide its own name, so we know what to call it.
    $field_id = $field_subform['#id'];
    $form['fields'][$field_id] = $field_subform;
  }

  // Now the fieldgroups.
  $form['fieldgroups'] = array(
    '#type'       => 'fieldset',
    '#title'      => t('Groups'),
    '#theme'      => 'field_schema_importer_fieldsets_as_table',
    // For js magic.
    '#attributes' => array('class' => array('typedef-groups')),
  );
  foreach ((array) @$data['fieldgroups'] as $group) {
    $subform = field_schema_importer_group_edit_form((array) $group);
    // The field renderer will provide its own name, so we know what to call it.
    $id = $subform['#id'];
    $form['fieldgroups'][$id] = $subform;
  }

  $form['validate_content_type'] = array(
    '#type'     => 'submit',
    '#value'    => t('Validate Content Type'),
    '#validate' => array('field_schema_importer_content_type_form_validate'),
    '#submit'   => array('field_schema_importer_content_type_form_validate_button'),
  );
  $form['submit_content_type'] = array(
    '#type'     => 'submit',
    '#value'    => t('Validate and create content type'),
    '#validate' => array('field_schema_importer_content_type_form_validate'),
    '#submit'   => array('field_schema_importer_content_type_form_submit'),
  );

  return $form;
}

/**
 * Provide an all-in-one edit subform for individual fields.
 *
 * A very cut-down version of what you find at /admin/content/node-type/x/fields
 *
 * This is called from the parent content type form
 * field_schema_importer_content_type_form()
 *
 * Provides edit fields that display the field info we can deal with.
 *
 * If we found an existing field def with exactly this name,
 * reflect that ones *current* settings
 * instead of the recommended/imported ones - but make a note FYI.
 *
 * For the UI, we merge the concepts of instance and field_def a little.
 *
 * Although we construct it as a fieldset, the preferred theme function will
 * render it in table rows later.
 *
 * @see content_field_basic_form()
 */
function field_schema_importer_field_edit_form($field, $instance, $groups_list = array()) {

  // Borrow some core field UI widgets.
  module_load_include('inc', 'field_ui', 'field_ui.admin');

  // For interest, see if the base field already exists.
  $prior_field = field_read_field($field['field_name'], array('include_inactive' => TRUE));
  $prior_instance = field_read_instance($instance['entity_type'], $instance['field_name'], $instance['bundle']);

  $field_type_options = field_ui_field_type_options();
  $widget_type_options = field_schema_importer_widget_type_options();

  // We insert some classes and hints about the individual fields
  // into the container for use with css and js.

  $form = array(
    '#type'       => 'fieldset',
    '#id'         => $field['field_name'],
    '#attributes' => array('class' => array()),
  );
  $form['create'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Create'),
    '#default_value' => isset($instance['create']) ? $instance['create'] : TRUE,
  );
  // Visual hint.
  $create_class = $form['create']['#default_value'] ? 'create' : 'no-create';
  $form['#attributes']['class'][] = $create_class;

  $exists = $prior_instance ? 'exists' : 'new';
  $form['exists'] = array(
    '#type'   => 'markup',
    '#markup' => "<small>($exists)</small>",
  );
  $form['#attributes']['class'][] = "field-$exists";

  if (!empty($field['uri'])) {
    $form['exists']['#suffix'] = l(t('#'), $field['uri']);
  }

  // If you have this field locally already, your label wins.
  $label = isset($prior_instance['label']) ? $prior_instance['label'] : $instance['label'];
  $form['label'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Label'),
    '#default_value' => $label,
    '#size'          => 24,
  );
  $form['field_name'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Machine name'),
    '#default_value' => $instance['field_name'],
    '#description'   => t("
      Changing the machine name will put this out of synch with the imported
      schema.
      The machine name shown here may be truncated from the given one,
      due to Drupal field API length constraints.
    "),
    '#size'          => 32,
    '#maxlength'     => 32,
  );

  $type_suffix = "";
  if (!in_array($field['type'], array_keys($field_type_options))) {
    // This means that we don't have enough widgets!
    // If you haven't turned on date or filefield, you suck.
    $type_suffix = "
      <small>I want to use <b>{$field['type']}</b> here!
      Please enable the appropriate field module.</small>
    ";
    $field['type'] = 'text';
  }
  $type_options = $field_type_options;
  // If you have this field locally already, your type wins.
  $form['type'] = array(
    '#type'          => 'select',
    '#title'         => t('Field type'),
    '#default_value' => isset($prior_field['type']) ? $prior_field['type'] : $field['type'],
    '#options'       => $type_options,
    '#attributes'    => array(
      'style' => 'width:12em',
      'class' => array('typedef-type'),
    ),
    // Class is for js magic.
    '#description'   => t('
      Only <em>suggested</em> types are pre-selected here, based on some of the
      hundreds available.
      You can and should choose a more specific one than "text" if you can.
    '),
    '#suffix'        => $type_suffix,
  );

  // Vocabulary and entityreference are bunched together in the form rendering
  // later.

  // Additional data for vocabulary selection.
  // Redundant if not a taxonomy, hidden through js.
  $vocabulary_options = field_schema_importer_vocabulary_options();
  $selected = @$prior_field['settings']['handler_settings']['target_bundles'];
  $form['vocabularies'] = array(
    '#type'          => 'select',
    '#title'         => t('Vocabulary'),
    '#options'       => $vocabulary_options,
    '#default_value' => $selected,
    '#attributes'    => array('style' => 'width:8em'),
    '#description'   => t("
      The available taxonomy vocabularies you can use if the field type is
      going to be handled as a tag.
    "),
  );

  // Additional data for references.
  // (redundant if not a reference, hidden through js/css)
  $entityreference_options = field_schema_importer_entity_type_options();

  $form['referenceable_types'] = array(
    '#type'        => 'select',
    '#title'       => t('Reference'),
    '#options'     => $entityreference_options,
    '#attributes'  => array('style' => 'width:8em'),
    '#description' => t("
      The suggested target type here shows what <em>sort</em> of data could go
      into this field.
      Often that would be a reference to another topic.
      If you want to support this structure, you must create or import the
      target content type before proceeding.
      <br/>Or set it to {new} and an empty content type definition will be created.
      <br/>Or set it as a entityreference to {none} for now and connect them up later.
    "),
  );

  if ($prior_field) {
    // Need to convert the way field saves the target bundle info into the
    // way that I expose it through the form, eg 'node:page'
    $selected_bundles = @$prior_field['settings']['handler_settings']['target_bundles'];
    $selected_entity_type = @$prior_field['settings']['target_type'];
    $selected = array();
    foreach ((array) $selected_bundles as $selected_bundle) {
      $selected_key = $selected_entity_type . ':' . $selected_bundle;
      $selected[$selected_key] = $selected_key;
    }
    $form['referenceable_types']['#default_value'] = $selected;
  }

  if (!empty($field['referenceable_types'])) {

    // Figure what to set as the default selection here,
    // based on string keys and things.

    // 2014 NEEDS WORK - the logic here needs review after the D6 upgrade.

    // Even though we may not be requiring you to use a entityreference
    // provide a hint about what type of data SHOULD go there
    // if you want to do something better than just 'text'.

    // Fetch an index of entities (content types) that are registered as being
    // labelled with any rdf type.
    $rdf_mapping_bundles_by_uri = function_exists('rdf_mapping_bundles_by_uri') ? rdf_mapping_bundles_by_uri() : array();
    // So, if this field has a match between its 'expected' field type and our
    // existing content types, pre-fill that as a probable target.
    $likely_matches = array();

    // Add clues to let the admin know about the data types,
    // and assist autocreation. The referenceable_types on a field may contain
    // Links to the content definition, if there are absolute docs for it.
    $referenceable_links = array();
    foreach ($field['referenceable_types'] as $type => $link) {
      $referenceable_links[$type] = valid_url($link) ? l($type, $link) : $type;
      if (!empty($rdf_mapping_bundles_by_uri[$link])) {
        // Found a content type with the same rdf mapping identifier as our
        // 'expected' type.
        // That's a good match.
        $likely_matches += $rdf_mapping_bundles_by_uri[$link];
      }
      // Also, see if we can match by machine_name/type_id
      // - this still works if you don't have rdf_mapping enabled.
      if (in_array($type, $field['referenceable_types'])) {
        $likely_matches += array($type);
      }
    }

    $form['referenceable_types']['#suffix'] = "<small>Expects {" . implode(',', $referenceable_links) . "}</small>";
    if (empty($form['type']['#default_value'])) {
      $form['referenceable_types']['#default_value'] = $likely_matches;
    }
    if (!empty($likely_matches)) {
      $form['type']['#default_value'] = 'entityreference';
    }
  }
  // Finished entityreference additions.

  // If you have this field locally already, your widget wins.
  // Otherwise set it to something appropriate to the field.
  $widget_type = isset($prior_instance['widget']['type']) ? $prior_instance['widget']['type'] : $instance['widget']['type'];
  $form['widget_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Widget type'),
    '#default_value' => $widget_type,
    '#options'       => $widget_type_options,
    '#attributes'    => array(
      'style' => 'width:12em',
      // For js magic.
      'class' => array('typedef-widget-type'),
    ),
    '#description'   => t('
      You must use the correct widget for the field type.
      You can change this later.
    '),
  );

  $form['group'] = array(
    '#type'          => 'select',
    '#title'         => t('Group'),
    '#default_value' => @$instance['group'],
    '#options'       => $groups_list,
    '#description'   => t('
      Field grouping reflects the sub-topic (freebase included_types)
      that may go into making up a type definition. You can change this later.
    '),
  );
  $form['#attributes']['class'][] = "group-" . @$field['group'];

  // Pass this through as a hidden field.
  $form['expected_type'] = array(
    '#type'          => 'hidden',
    '#default_value' => @reset(array_keys($field['referenceable_types'])),
  );

  return $form;
}

/**
 * List of available vocabs, plus 'new' option. For select fields.
 *
 * @return array
 *   A list of vocabs for use in a select box.
 */
function field_schema_importer_vocabulary_options() {
  static $vocabulary_options;
  if (isset($vocabulary_options)) {
    return $vocabulary_options;
  }

  $vocabularies = taxonomy_get_vocabularies();
  $vocabulary_options = array('' => '<none>');
  foreach ($vocabularies as $vocabulary) {
    $vocabulary_options[$vocabulary->vid] = check_plain($vocabulary->name);
  }
  $vocabulary_options['new'] = '<new>';
  return $vocabulary_options;
}

/**
 * Prepare the list of referencable entities (code from entityreference).
 *
 * @return array
 *   Suitable for using with a select field.
 */
function field_schema_importer_entity_type_options() {
  $entity_type_options = array('' => '<none>');
  $creatable = field_schema_importer_createable_bundles();
  foreach (entity_get_info() as $entity_type => $entity_info) {
    $entity_type_options[$entity_info['label']] = array();
    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      // Key needs to include the entity type as well as the bundle ID.
      $key = $entity_type . ':' . $bundle_name;
      $entity_type_options[$entity_info['label']][$key] = $bundle_info['label'];
    }
    // 'New' needs to be per entity type, but is only useful for entities that
    // we actually know how to build bundles for.
    if (!empty($creatable[$entity_type]['bundle_save_callback'])) {
      $new_key = $entity_type . ':new';
      $entity_type_options[$entity_info['label']][$new_key] = '<new>';
    }
  }
  return $entity_type_options;
}


/**
 * Prepare the list of referencable widgets.
 *
 * @see field_ui_widget_type_options()
 *
 * @return array
 *   suitable for using with a select field.
 */
function field_schema_importer_widget_type_options() {
  $widget_type_options = field_ui_widget_type_options(NULL, TRUE);
  // Small UI tweak. I prefer autocomplete to be the default widget for
  // entityreference. Jiggle the sort order so that happens.
  // Same for other widgets.
  $preferred_widgets = array(
    'entityreference' => 'entityreference_autocomplete',
    'text'            => 'text_textfield',
    'date'            => 'date_text',
    'datestamp'       => 'date_text',
    'datetime'        => 'date_text',
  );
  foreach ($preferred_widgets as $datatype => $preferred_widget) {
    if (isset($widget_type_options[$datatype])) {
      $opts = & $widget_type_options[$datatype];
      $label = $opts[$preferred_widget];
      unset($opts[$preferred_widget]);
      // Shift that preferred widget to the front of the list.
      $opts = array($preferred_widget => $label) + $opts;
    }
  }
  return $widget_type_options;
}


/**
 * Subform for defining fieldUI fieldgroups.
 *
 * @param array $group
 *   Group definitions
 *
 * @return array
 *   FAPI subform
 */
function field_schema_importer_group_edit_form($group) {
  $form = array(
    '#type' => 'fieldset',
    '#id'   => $group['identifier'],
  );
  $form['create'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Create'),
    '#default_value' => TRUE,
  );
  $form['label'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Label'),
    '#default_value' => $group['label'],
    '#size'          => 24,
  );
  $form['group_name'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Machine name'),
    '#value'    => $group['group_name'],
    '#disabled' => TRUE,
    '#size'     => 32,
  );

  // If there was any other data in the cck field def we were given
  // ... retain it using form persistence.
  foreach ($group as $attname => $attval) {
    if (!isset($form[$attname])) {
      $form[$attname] = array(
        '#type'  => 'value',
        '#value' => $attval,
      );
    }
  }

  return $form;
}


/**
 * Render the fields in an all-in-one edit form - as a table.
 *
 * The form was built as a nested fieldset, turn it into a table
 *
 * This takes a 'fieldset' and re-renders all the elements into rows and
 * columns.
 *
 * TODO, should this be using #type='tableselect' instead, that seems to
 * give us the 'select-all' toggle, but at what cost?
 *
 * @param array $variables
 *   A FAPI subform - a fieldset containing fieldsets containing form
 *   fields - which become table, rows and cells respectively.
 *
 * @return string
 *   HTML
 */
function theme_field_schema_importer_fieldsets_as_table($variables) {
  $element = $variables['element'];
  $table = array();
  // Fields_container is a fieldset, that will become a table.
  $row_ids = element_children($element);

  // Auto-gen the table header.
  $header = array();
  $footer = array();
  $sample_row = array();

  // Aggregate ALL rows before creating the header,
  // in case they have unequal number of elements (cols).
  foreach ($row_ids as $fid) {
    $sample_row += $element[$fid];
  }
  $col_ids = element_children($sample_row);
  foreach ($col_ids as $col_id) {
    $sample_col = $sample_row[$col_id];
    if ($sample_col['#type'] == 'value' || $sample_col['#type'] == 'hidden') {
      continue;
    }
    $header[$col_id] = array('data' => @$sample_col['#title']);
    $footer[$col_id] = @$sample_col['#description'];
  }

  // Vocabularies are bunched in with entityreference, no column label.
  unset($header['vocabularies']);
  unset($footer['vocabularies']);

  foreach ($row_ids as $fid) {
    $field_container = $element[$fid];
    // $field_container is a fieldset that will become a row.
    $table[$fid] = array();
    // Copy a few of the form attributes into this row style.
    $table[$fid] += $field_container['#attributes'];

    foreach (element_children($field_container) as $col_id) {
      $form_field = & $element[$fid][$col_id];
      // Keep a handle so the element remembers when it's been rendered.
      // $form_field is the actual HTML field that will become a cell.
      unset($form_field['#title']);
      unset($form_field['#description']);

      // Special case, lump the taxonomy selection and the entityreference
      // selection together.
      // This cell should only ever be showing neither or one.
      // of these select boxes.
      // They are very similar functions.
      if ($col_id == 'referenceable_types' || $col_id == 'vocabularies') {
        if (!isset($table[$fid]['data']['referenceable_types-vocabularies'])) {
          $table[$fid]['data']['referenceable_types-vocabularies'] = array(
            'data'  => '',
            'class' => array('referenceable_types-vocabularies'),
          );
        }
        $table[$fid]['data']['referenceable_types-vocabularies']['data'] .= "<div class=\"$col_id\">" . drupal_render($form_field) . '</div>';
      }
      else {
        // #value fields cause empty cells.
        if ($form_field['#type'] != 'value') {
          $table[$fid]['data'][$col_id] = array(
            'data'  => drupal_render($form_field),
            // Name our columns for js and css.
            'class' => array($col_id),
          );
        }
      }

    }
    // Every col.
  }
  // Every row.

  // Add the descriptions in a bottom row.
  $table['footer'] = array(
    'data'  => array(),
    'class' => array('footer'),
  );
  foreach ($footer as $col_id => $footer_description) {
    $table['footer']['data'][$col_id] = '<small class="description">' . $footer_description . '</small>';
  }

  // UI extra (only lightly tested)
  // Insert a select-all column - using js stolen from tableselect.
  $header['create']['class'][] = 'select-all';
  // Give it some space.
  unset($header['exists']);
  $header['create']['colspan'] = '2';

  // Core tablesort complains if I use keys for the cols instead of numbers.
  // Damn silly. Work around it to avoid a strict notice.
  $header[0] = '';
  return theme('table', array(
    'header'     => $header,
    'rows'       => $table,
    'attributes' => $element['#attributes'],
  ));
}


/**
 * Button handler for the 'validate' button.
 *
 * Do nothing, but need to be handled otherwise 'default' submission happens.
 *
 * @param array $form
 *   FAPI
 * @param array $form_state
 *   FAPI
 */
function field_schema_importer_content_type_form_validate_button($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  // dpm(get_defined_vars());
}

/**
 * Submitting the field_schema_importer_content_type_form().
 *
 * This will merge any changes made in the edit form back onto the parsed_data.
 *
 * This is called from a submit button #validate so the $form is not much help.
 */
function field_schema_importer_content_type_form_validate($form, &$form_state) {
  module_load_include('inc', 'field_schema_importer', 'field_schema_importer.process');

  // Take whatever was remembered in the $form_state parsed data array
  // and apply whatver was submitted in the form_values over top of it.
  // This produces a new config set.
  // Validate that.

  $previous_data = $form_state['storage']['parsed_result'];
  // Bundles are objects, just to be fiddly.
  foreach ($previous_data['bundles'] as $i => $bundle) {
    $previous_data['bundles'][$i] = (array) $previous_data['bundles'][$i];
  }
  $submitted_data = $form_state['values']['preview'];

  // Full in expected blanks to avoid too many notices later.
  foreach (array('bundles', 'fields', 'instances', 'fieldgroups') as $key) {
    if (!isset($submitted_data[$key])) {
      $submitted_data[$key] = array();
    }
  }

  $new_data = array_replace_recursive($previous_data, $submitted_data);
  // Bundles are objects, just to be fiddly.
  foreach ($new_data['bundles'] as $i => $bundle) {
    $new_data['bundles'][$i] = (object) $new_data['bundles'][$i];
  }
  // Group is an object too, says field_group.module.
  foreach ($new_data['fieldgroups'] as $i => $group) {
    $new_data['fieldgroups'][$i] = (object) $new_data['fieldgroups'][$i];
  }

  // That will have updated the bundles,
  // and updated the field DEFs but not the field instances.
  // Some keys belong in the instance, not the field def.
  // Imported schemas usually bunch them together.
  // That needs to be copied over.
  foreach ($new_data['instances'] as $field_id => &$instances) {
    foreach ($instances as $i => &$instance) {
      if (empty($instance)) {
        // May have already been filtered out.
        continue;
      }
      $field = & $submitted_data['fields'][$field_id];
      if (empty($field['create'])) {
        // Remove the fields we don't want to create.
        unset($new_data['fields'][$field_id]);
        unset($instances[$i]);
        continue;
      }
      // Otherwise, copy field info over.
      $instance['label'] = $field['label'];
      $instance['description'] = @$field['description'];
      $instance['required'] = @$field['required'];
      $instance['widget']['type'] = $field['widget_type'];

      if ($field['field_name'] != $field_id) {
        // It looks like you are tying to rename a field machine name.
        // Tricky, but lets try.
        $new_field_id = $field['field_name'];
        $new_data['instances'][$new_field_id] = $new_data['instances'][$field_id];
        $new_data['instances'][$new_field_id][$i]['field_name'] = $new_field_id;
        // The display renderers will now be invalid for a different content
        // type.
        unset($new_data['instances'][$new_field_id]['display']);
        unset($new_data['instances'][$field_id]);
        $new_data['fields'][$new_field_id] = $new_data['fields'][$field_id];
        unset($new_data['fields'][$field_id]);
        // Good luck!
      }
    }
  }

  // $new_data should now be an array used by bundle_copy.

  // Validate the bundle again.
  // Ensure that the selected widgets are valid for this field type etc,
  // Perform tidy-up of form values.
  foreach ($new_data['bundles'] as $i => &$bundle) {
    if (!field_schema_importer_validate_content_type($bundle)) {
      form_set_error("bundles][$i][bundle", "User-modified content type definition did not validate");
      drupal_set_message("User-modified content type definition did not validate");
    }
  }
  field_schema_importer_validate_fields($new_data['fields'], $new_data['instances']);

  $form_state['storage']['validated_result'] = $new_data;
}

/**
 * Here at the end we actually create the type.
 *
 * Will only trigger if the validate came through clean.
 */
function field_schema_importer_content_type_form_submit($form, &$form_state) {
  module_load_include('inc', 'field_schema_importer', 'field_schema_importer.process');

  // $form_values = $form_state['values'];
  // The validation func should have cooked the data and re-saved it into
  // $form_state['storage']['validated_result'] = $new_data;
  $data = $form_state['storage']['validated_result'];

  // Last minute, add this thing that bundle_copy demands.
  // Can remove this if I replace bundle_copy.
  foreach ($data['bundles'] as $key => $bundle) {
    if (empty($data['bundles'][$key]->bc_entity_type)) {
      $data['bundles'][$key]->bc_entity_type = 'node';
    }
  }

  drupal_set_message(t("Importing a content type."));
  return field_schema_importer_create_bundles($data);
}
