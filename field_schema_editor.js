/**
 * @file UI addition to enhance the field table.
 * @author 'dman' Dan Morrison http://coders.co.nz/
 * @version 2009-10
 */
(function ($) {

/**
 * Add and modify classes on each row in a fields table
 * this will enable us to hide elements in the table if we want, depending on the selected type
 * - the entityreference target in this case
 */
Drupal.behaviors.schema_row_filter = {
  attach: function() {
  $('.typedef-fields tr').addClass('filtered');
  // Any time you change the type of a field, update the row to note that.
  // Certain types need to show or hide certain elements. 
  // This is then done with css, keyed on the new row style.
  // See field_editor.css for those rules.
  $('.typedef-fields tr .typedef-type').change(
    function(){
      var row = $(this).parents('tr');
      // remove all and any 'types' from the previous class
      var classes = row.attr('class');
      var new_classes = classes.replace(/type\-.*/, ''); 
      row.attr('class', new_classes);
      row.addClass('type-' + this.value);
    }
  );
  // Trigger the filter to update the current display.
  $('.typedef-fields tr .typedef-type').trigger('change');
}}

Drupal.behaviors.schema_row_toggle = {
  attach: function() {
  $('.typedef-fields tr .create input').change(
    function(){
      var row = $(this).parents('tr');
      if ($(this).attr('checked')) {
        row.removeClass('no-create');
        row.addClass('create')
      }
      else {
        row.removeClass('create');
        row.addClass('no-create')
      }
    }
  );
  // Trigger the filter to update the current display.
  // Not working in d7?
  $('.typedef-fields tr .create input').trigger('change');
}}

Drupal.behaviors.schema_widget_checker = {
  attach: function() {
  $('.typedef-fields tr .typedef-type').change(
    /**
     * Ensure the selected widget option is right for this data type
     * The options are within keyed optgroups which we can look at
     */ 
    function() {
      var row = $(this).parents('tr');
      var field_type = $('.typedef-type' , row).val();
      var field_label = $('.typedef-type option:selected' , row).text();
      var selected_optgroup = $('.typedef-widget-type', row).find("option:selected").parent().attr("label")
      if (selected_optgroup != field_type) {
        // Select the first option in the optgroup with the label we now want.
        console.log("Need to set widget to " + selected_optgroup + " at option inside field_type "+ field_type);
        $('.typedef-widget-type optgroup[label="'+ field_label +'"] option:first-child', row).attr('selected', 'selected')
        $('.typedef-widget-type', row)
          .animate({opacity:.5}, 200)
          .animate({opacity:1}, 200)
      }
    }
  )

  $('.typedef-fields tr .typedef-widget-type').change(
    /**
     * If you changed the widget, ensure the type matches our current optgroup
     */
    function() {
      var row = $(this).parents('tr');
      var field_type = $('.typedef-type' , row).val();
      var selected_widget = $('.typedef-widget-type', row).find("option:selected").val();
      var selected_optgroup = $('.typedef-widget-type', row).find("option:selected").parent().attr("label")
      if (selected_optgroup != field_type) {
        // FIX it.
        console.log("Need to set field type to "+ selected_optgroup + " because the widget changed to " + selected_widget);
        // Select the field type matching our current optgroup.
        // Need to find the field type with a label that matches, not machine name.
        // ugly http://stackoverflow.com/questions/3744289/jquery-how-to-select-an-option-by-its-text
        var field_machinename =  $(".typedef-type option", row).filter(function(){return $(this).html() == selected_optgroup}).val();
        $('.typedef-type', row).val(field_machinename)
          .animate({opacity:.5}, 200)
          .animate({opacity:1}, 200)
      }
    }
  )

  // Trigger the filter to update the current display
  $('.typedef-fields tr .typedef-type').trigger('change');
}}


})(jQuery);